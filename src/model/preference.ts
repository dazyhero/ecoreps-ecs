

export enum Preference{
    lastSummary = "lastsummary",
    lastAssignment = "lastAssignment",
    lastExam = "lastExam",
    selectedUniversityId =  "selectedUniversityId",
    nonce =  "nonce"
}