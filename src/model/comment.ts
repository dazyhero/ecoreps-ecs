import { IChapter } from './chapter';
export enum CommentVisibility {
  admin,
  published,
  deleted,
}

export const AllCommentVisibilities = [
  CommentVisibility.published,
  CommentVisibility.admin,
  CommentVisibility.deleted,
];
export default interface IComment {
  _id: any;
  author_id: string;
  author_name: string;
  title: string | void;
  text: string;
  date: Date;
  visibility: CommentVisibility;
  replies: IComment[];
  chapter: string | IChapter;
  code?: number;
}
