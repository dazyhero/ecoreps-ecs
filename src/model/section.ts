/**
 * Created by huck on 15.06.18.
 */

import IComment from './comment';

export enum SectionType {
  /* -- Summary ---*/
  //Definition
  Definition = 'Definition',
  //Formel
  Rules = 'Rules',
  //Formel ohne !
  Rules2 = 'Rules2',
  // Beispiel
  Example = 'Example',
  // Vorgehen
  Steps = 'Steps',
  //Tipps
  Tipps = 'Tipps',
  //Text
  Text = 'Text',
  // Regel
  Rule = 'Rule',
  // Tipp
  Tip = 'Tip',
  // Herleitung
  Derivation = 'Derivation',
  // Streberwissen
  NerdKnowledge = 'NerdKnowledge',
  /* --- Exam --- */
  ExamSolution = 'ExamSolution',
  HighlightedExamSolution = 'HighlightedExamSolution',
  ExamText = 'ExamText',
}

export const ChapterSectionTypes = [
  SectionType.Text,
  SectionType.Rules,
  SectionType.Definition,
  SectionType.Example,
  SectionType.Steps,
  SectionType.Tipps,
  SectionType.Tip,
  SectionType.Rules2,
  SectionType.Rule,
  SectionType.Derivation,
  SectionType.NerdKnowledge,  
];
export const AssignmentSectionTypes = [
  SectionType.ExamSolution,
  SectionType.HighlightedExamSolution,
  SectionType.ExamText,
];

export const AllSectionTypes = ChapterSectionTypes.concat(
  AssignmentSectionTypes,
);

export enum SectionImagePosition {
  insideBox = 0,
  belowBox = 1,
  aboveBox = 2,
}

export interface ISectionTypeInfo {
  defaultTitle: string;
  imagePosition: SectionImagePosition;
  cssClasses: string[];
  boxCssClasses: string[];
  classString: string;
  boxClassString: string;
  expandable: boolean;
  type: SectionType;
}

export class SectionTypeInfo implements ISectionTypeInfo {
  constructor(public type: SectionType) {}

  public get classString(): string {
    return this.cssClasses.join(' ');
  }

  public get boxClassString(): string {
    return this.boxCssClasses.join(' ');
  }

  public get includeInProgress(): boolean {
    return  [SectionType.ExamText].indexOf(this.type) === -1;
}

public get includeSections(): boolean {
  return  [SectionType.ExamText].indexOf(this.type) === -1;
}
  public get defaultTitle(): string {
    const titles = {
      [SectionType.Definition]: 'Definition',
      [SectionType.Rules]: 'Formel',
      [SectionType.Rules2]: "Formel ohne '!'",
      [SectionType.Example]: 'Beispiel',
      [SectionType.Steps]: 'Vorgehen',
      [SectionType.Tipps]: 'Merke!',
      [SectionType.Tip]: 'Tipp',
      [SectionType.Rule]: 'Regel',
      [SectionType.Derivation]: 'Herleitung',
      [SectionType.NerdKnowledge]: 'Streberwissen',
      [SectionType.ExamSolution]: 'Lösung',
      [SectionType.HighlightedExamSolution]: 'Hervorgehobene Lösung',
      [SectionType.ExamText]: 'Aufgabentext',
    };

    return titles[this.type] || '';
  }

  public get expandable(): boolean {
    return (
      [SectionType.ExamSolution, SectionType.HighlightedExamSolution].indexOf(
        this.type,
      ) >= 0
    );
  }

  public get cssClasses(): string[] {
    const classes = {
      [SectionType.Definition]: ['definition-body'],
      [SectionType.Rules]: [
        'rules-body',
        'col-md',
        'container-fluid',
        'bodybox',
      ],
      [SectionType.Rules2]: [
        'rules-body',
        'col-md',
        'container-fluid',
        'bodybox',
      ],
      [SectionType.Rule]: [
        'rules-body',
        'col-md',
        'container-fluid',
        'bodybox',
      ],
      [SectionType.Steps]: ['example-box', 'steps'],
      [SectionType.Example]: ['example-box'],
      [SectionType.Tipps]: ['tipps-body', 'container-flex'],
      [SectionType.Tip]: ['tipps-body', 'container-flex', 'tip'],
      [SectionType.Derivation]: ['example-box'],
      [SectionType.NerdKnowledge]: ['tipps-body', 'container-flex', 'nerd-knowledge'],
      [SectionType.ExamSolution]: [],
      [SectionType.HighlightedExamSolution]: ['rules-body', 'container-flex'],
    };

    return classes[this.type] || [];
  }

  public get boxCssClasses(): string[] {
    const classes = {
      [SectionType.ExamSolution]: ['example-box'],
      [SectionType.HighlightedExamSolution]: ['example-box'],
    };

    return classes[this.type] || [];
  }

  public get imagePosition(): SectionImagePosition {
    const positions = {};
    return positions[this.type] || SectionImagePosition.insideBox;
  }
}

//TODO: disambiguate array usage [string] vs ["a b"]
export function classesForChapterSectionType(
  sectionType: SectionType,
): string[] {
  switch (sectionType) {
    case SectionType.Example:
      return ['example-box'];
    case SectionType.Definition:
      return ['definition-body'];
    case SectionType.Steps:
      return ['example-box'];
    case SectionType.Tipps:
      //   alert("im a tipp")
      return ['tipps-body container-flex'];
    case SectionType.Rules:
      return ['rules-body container-flex'];
    case SectionType.Rules2:
      return ['rules-body container-flex'];
    default:
      return [];
  }
}

export function defaultTitleForChapterSectionType(
  sectionType: SectionType,
): string {
  switch (sectionType) {
    case SectionType.Definition:
      return 'Definition';
    case SectionType.Rules:
      return 'Formel';
    case SectionType.Rules2:
      return "Formel ohne '!'";
    case SectionType.Example:
      return 'Beispiel';
    case SectionType.Steps:
      return 'Vorgehen';
    case SectionType.Tipps:
      return 'Merke!';
    case SectionType.Tip:
      return 'Tipp';
    case SectionType.Rule:
      return 'Regel';
    case SectionType.Derivation:
      return 'Herleitung';
    case SectionType.NerdKnowledge:
      return 'Streberwissen';

    default:
      return null;
  }
}

export enum IIllustrationSize {
  small = 0,
  medium = 1,
  large = 2,
}

export type IIllustrationPixelSize = {
  height: number;
  width: number;
}

export enum IVideoDisplayMode {
  collapsed = 0,
  expanded = 1,
  playing = 2,
}

export interface IIllustrationWrapper {
  url?: string;
  illustration?: string;
  description?: string;
  size: IIllustrationSize;
  pixelSize: IIllustrationPixelSize;
}

export interface IVideoWrapper {
  url?: string;
  public_id?: string;
  description?: string;
  displayMode: IVideoDisplayMode;
  isVimeo?: boolean;
}

export interface ISection {
  _id: any;
  title?: string;
  hasTitle?: boolean;
  type?: SectionType;
  content?: string;
  images?: IIllustrationWrapper[];
  mobileImages?: IIllustrationWrapper[];
  videos?: IVideoWrapper[];
  fullURLs?: string[];
  editorContent?: string;
  editorTitle?: string;
  comments?: IComment[] | string[];
  stats?: {
    commentCount: number;
  };
}

