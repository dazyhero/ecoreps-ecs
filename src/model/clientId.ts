export enum ClientId {
  ecoreps = 100,
  studyPrime = 200,
}
export type ClientType = 'Ecoreps' | 'StudyPrime';

export const ClientTypeId: Record<number, ClientType> = {
  100: 'Ecoreps',
  200: 'StudyPrime',
};

