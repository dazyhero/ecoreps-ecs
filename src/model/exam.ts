import { ISection } from './section';

export interface IExamSection extends ISection {}

export interface IExamAssignment {
  _id: any;
  title: string;
  sections: IExamSection[];
  url_slug: string;
  order?: number;
}

export interface IExam {
  _id: any;
  title: string;
  order: number;
  url_slug: string;
  assignments: IExamAssignment[];
}

export interface ICreateExamModel {
  title: string;
}

export interface ICreateExamAssignmentModel {
  title: string;
}
export interface IUpdateExamModel {
  _id: string;
  title: string;
}
export interface IUpdateExamAssignmentModel {
  _id: string;
  title: string;
}
