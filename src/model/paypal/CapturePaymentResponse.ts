export interface ICapturePaymentResponse {
    id?:             string;
    purchase_units?: PurchaseUnit[];
    payer?:          Payer;
    links?:          Link[];
    status?:         string;
}

export interface Link {
    href?:   string;
    rel?:    string;
    method?: string;
}

export interface Payer {
    name?:          PayerName;
    email_address?: string;
    payer_id?:      string;
    address?:       PayerAddress;
}

export interface PayerAddress {
    country_code?: string;
}

export interface PayerName {
    given_name?: string;
    surname?:    string;
}

export interface PurchaseUnit {
    reference_id?: string;
    shipping?:     Shipping;
    payments?:     Payments;
}

export interface Payments {
    authorizations?: Authorization[];
}

export interface Authorization {
    status?:            string;
    id?:                string;
    amount?:            Amount;
    seller_protection?: SellerProtection;
    expiration_time?:   Date;
    links?:             Link[];
    create_time?:       Date;
    update_time?:       Date;
}

export interface Amount {
    currency_code?: string;
    value?:         string;
}

export interface SellerProtection {
    status?:             string;
    dispute_categories?: string[];
}

export interface Shipping {
    name?:    ShippingName;
    address?: ShippingAddress;
}

export interface ShippingAddress {
    address_line_1?: string;
    admin_area_2?:   string;
    admin_area_1?:   string;
    postal_code?:    string;
    country_code?:   string;
}

export interface ShippingName {
    full_name?: string;
}
