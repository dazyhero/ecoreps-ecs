export interface IContactRequest {
  _id: any;
  fullName: string;
  email: string;
  universityId: string;
  university: any;
  otherUniversity: string;
  subject: string;
  course: string;
  message: string;
  captcha: string;
}
