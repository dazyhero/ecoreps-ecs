import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { get as GetConfig } from 'config';
import cookieParser from 'cookie-parser';
import { ValidationPipe } from '@nestjs/common';

const initPort = GetConfig('Service.server.port');
const cors = GetConfig('Service.server.cors');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  app.enableCors({ origin: cors });
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix('v2');
  await app.listen(initPort);
}
bootstrap();
