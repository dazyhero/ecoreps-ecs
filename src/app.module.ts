import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';

import { SharedModule } from './modules/shared/shared.module';
import { ProfileModule } from './modules/profile/profile.module';
import { MongooseModule as MongooseCustomModule } from './modules/mongoose/mongoose.module';
import { get as GetConfig } from 'config';
import { MongooseModule } from '@nestjs/mongoose';
import { profileProvider } from './modules/profile/profile.provider';

import { UserClaimMiddleware } from './modules/shared/middlewares/user-claim.middleware';
import { OrderModule } from './modules/order/order.module';
import { ClientIdMiddleware } from './modules/shared/middlewares/clientId.middleware';
const dbURL = process.env.MONGO_URL || GetConfig('Service.Database.connectionString');
const ecorepsDb = GetConfig('Service.Database.ecoreps');
const studyprimeDb = GetConfig('Service.Database.studyprime');

@Module({
  imports: [
    SharedModule,
    ProfileModule,
    OrderModule,
    MongooseCustomModule,
    MongooseModule.forRoot(`${dbURL}/${ecorepsDb}`, {
      connectionName: ecorepsDb,
    }),
    MongooseModule.forRoot(`${dbURL}/${studyprimeDb}`, {
      connectionName: studyprimeDb,
    }),
  ],
  controllers: [],
  providers: [...profileProvider],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(ClientIdMiddleware)
      .exclude(
        { path: 'profile/health', method: RequestMethod.GET },
        { path: 'v2/profile/health', method: RequestMethod.GET },
      )
      .forRoutes({
        path: '*',
        method: RequestMethod.ALL,
      })
      .apply(UserClaimMiddleware)
      .exclude(
        { path: 'profile/health', method: RequestMethod.GET },
        { path: 'v2/profile/health', method: RequestMethod.GET },
        { path: 'profile/get-anonymous-token', method: RequestMethod.GET },
        { path: 'v2/profile/get-anonymous-token', method: RequestMethod.GET },
      )
      .forRoutes({
        path: '*',
        method: RequestMethod.ALL,
      });
  }
}
