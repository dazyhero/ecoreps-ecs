export const DB_CONNECT_INJECTION = 'CORE_DB_CONNECTION';
export const PROFILE_MODEL = 'profiles';
export const COURSE_MODEL = 'courses';
export const USEDVOUCHER_MODEL = 'used_vouchers';
export const CHAPTER_MODEL = 'chapters';
export const ORDER_MODEL = 'orders';
