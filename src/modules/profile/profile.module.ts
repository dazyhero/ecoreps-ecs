import { Module } from '@nestjs/common';
import { ProfileController } from './controllers/profile.controller';
import { ProfileService } from './services/profile.service';
import { ProgressService } from './services/progress.service';
import { MongooseModule as MongooseCustomModule } from '../mongoose/mongoose.module';
import { profileProvider } from './profile.provider';
import { coursesProvider } from '../shared/providers/courses.provider';
import { chapterProvider } from '../shared/providers/chapter.provider';
import { DirectusClient } from '../shared/services/directus.client';
import { ProfileRespository } from './repository/profile.repository';

@Module({
  imports: [MongooseCustomModule],
  controllers: [ProfileController],
  providers: [ProfileService, ProgressService, DirectusClient, ...profileProvider, ...coursesProvider, ...chapterProvider, ProfileRespository],
})
export class ProfileModule {}
