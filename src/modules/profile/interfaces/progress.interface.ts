export type ColumnsProgressItemsNumbers = Record<string, number>;

export type ClassAverageProgress = {
  [id: string]: {
    progressUnderstood: number;
    progressProcessed: number;
  };
};

export type ColumnsProgressMeta = {
  totalProgressItems: number;
  progressItems: ColumnsProgressItemsNumbers;
};
