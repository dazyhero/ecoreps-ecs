import mongoose, { Connection } from 'mongoose';
import { DB_CONNECT_INJECTION, PROFILE_MODEL } from '../constants/db';

const completionSchema = new mongoose.Schema(
  {
    chapter: { type: mongoose.Schema.Types.ObjectId, required: true, reference: 'Chapter' },
    sections: [
      {
        section: { type: mongoose.Schema.Types.ObjectId, required: false, reference: 'Section' },
        sectionTitle: { type: mongoose.Schema.Types.String, required: false },
        state: { type: mongoose.Schema.Types.Number, required: false },
      },
    ],
    state: { type: mongoose.Schema.Types.Number, required: false },
  },
  { _id: false },
);

const columnProgress = new mongoose.Schema(
  {
    columnId: { type: mongoose.Schema.Types.String, required: true },
    progressUnderstood: { type: mongoose.Schema.Types.Number, required: true, default: 0 },
    progressProcessed: { type: mongoose.Schema.Types.Number, required: true, default: 0 },
    chapterProgress: { type: [completionSchema] },
    includeInAggregations: { type: mongoose.Schema.Types.Boolean, required: true, default: false },
  },
  { _id: false },
);

const progressSchema = new mongoose.Schema(
  {
    course_id: { type: mongoose.Schema.Types.ObjectId, required: true, reference: 'Course' },
    columnProgress: { type: [columnProgress] },
    // This is progress understood total
    progressTotal: { type: mongoose.Schema.Types.Number, default: 0 },
    progressProcessedTotal: { type: mongoose.Schema.Types.Number, default: 0 },
  },
  { _id: false, useNestedStrict: false },
);

export const UniversityRef = new mongoose.Schema({
  id: { type: String, default: null },
  url_slug: { type: String, default: null },
  name: { type: String, default: null },
});
export const ProfileSchema = new mongoose.Schema(
  {
    bookmarkedCourses: Array,
    user_id: { type: String, required: true, unique: true },
    last_login: { type: Number },
    forcedLogouts: { type: [Number], required: false, default: [] },
    progress: [progressSchema],
    anonymousId: { type: String, default: null },
    mainUniversity: UniversityRef,
  },
  {
    timestamps: true,
  },
);

export const profileProvider = [
  {
    provide: PROFILE_MODEL,
    useFactory: (connection: Connection) => {
      return connection.model(PROFILE_MODEL, ProfileSchema);
    },
    inject: [DB_CONNECT_INJECTION],
  },
];
