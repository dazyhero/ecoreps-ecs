import { Controller, Get, Delete, Param, Post, Body, Headers } from '@nestjs/common';
import { UserClaimData, UserId, IsUserAdmin } from '../../shared/decorators/user-claim.decorator';
import { ProfileService } from '../services/profile.service';
import { ProgressService } from '../services/progress.service';
import { IAuth0User } from '../../shared/types/user.types';

@Controller('profile')
export class ProfileController {
  constructor(private profileService: ProfileService, private progressService: ProgressService) {}

  @Get()
  public getUserData(@UserClaimData() userData): IAuth0User {
    return userData;
  }

  @Get('courses')
  public async getBookmarkedCourses(
    @UserClaimData() userData: IAuth0User,
    @UserId() userId: string,
    @IsUserAdmin() isAdmin: boolean,
  ) {
    return await this.profileService.getAvailableCourses(userId, userData, isAdmin);
  }

  @Post('courses/:courseId')
  public bookmarkCourse(
    @UserClaimData() userData: IAuth0User,
    @Param('courseId') courseId: string,
    @IsUserAdmin() isAdmin: boolean,
  ) {
    return this.progressService.bookmarkCourse(courseId, userData, isAdmin);
  }

  @Get('/health')
  public health() {
    return;
  }

  @Get('me')
  public getMyProfile(@UserId() userId: string): Promise<IAuth0User> {
    return this.profileService.getMyProfile(userId);
  }
  @Get('me2')
  public getMyProfile2(@UserId() userId: string): Promise<IAuth0User> {
    return this.profileService.getMyProfile(userId);
  }

  @Delete('courses/:courseId')
  public removeBookmarkedCourse(
    @UserId() userId: string,
    @IsUserAdmin() isAdmin: boolean,
    @Param('courseId') courseId: string,
  ) {
    return this.progressService.removeBookmarkedCourses(courseId, userId, isAdmin);
  }

  @Get('progress')
  public getMyProgress(@UserId() userId: string): Promise<any> {
    return this.progressService.getProgress(userId);
  }

  @Post('progress/:courseId')
  public complete(
    @Body() body,
    @UserClaimData() userData: IAuth0User,
    @Param('courseId') courseId: string,
    @IsUserAdmin() isAdmin: boolean,
  ) {
    return this.progressService.complete(body, userData, courseId, isAdmin);
  }

  @Delete('progress/:courseId/:columnId?')
  public resetProgress(
    @UserClaimData() userData: IAuth0User,
    @Param() params: { courseId: string; columnId?: string },
  ) {
    const { courseId, columnId } = params;
    return this.progressService.resetProgress(userData, courseId, columnId);
  }
  @Get('progress/:courseId/:chapterId?')
  public myProgress(@UserClaimData() userData: IAuth0User, @Param() params) {
    const { courseId, chapterId = null } = params;
    return this.progressService.myProgress(userData, courseId, chapterId);
  }

  @Get('session')
  public verifySession() {
    return true;
  }
 
  @Get('get-anonymous-token')
  public anonymous(@Headers('authorization') token) {
    return this.profileService.getOrCreateAnonymous(token);
  }

  @Post('university/:universityId')
  public setMainUniversity(
    @UserClaimData() userData: IAuth0User,
    @Param('universityId') universityId: string,
    @IsUserAdmin() isAdmin: boolean,
  ) {
    return this.profileService.setMainUniversity(universityId, userData, isAdmin);
  }

}

