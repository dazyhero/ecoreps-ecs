import _ from 'lodash';
import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { IAuth0User, ICourse, IChapter } from '../../shared/types/user.types';
import { ANONYMOUS_PREFIX, courseByIdLOrSlugQuery } from '../../../../shared/src';

import { PROFILE_MODEL, COURSE_MODEL, CHAPTER_MODEL } from '../../constants/db';
import { ClassAverageProgress } from '../interfaces/progress.interface';

@Injectable()
export class ProfileRespository {
  constructor(
    @Inject(PROFILE_MODEL) public profileModel: Model<IAuth0User>,
    @Inject(COURSE_MODEL) public courseModel: Model<ICourse>,
    @Inject(CHAPTER_MODEL) public chapterModel: Model<IChapter>,
  ) {}

  public getPurchasedCourses(courseId: string) {
    return this.profileModel.find({
      purchasedCourses: { $in: [courseId] },
    });
  }

  public getByUserId(userId: string) {
    return this.profileModel.findOne({ user_id: userId }).exec();
  }

  public findByCourseOrSlug(courseIdOrSlug: string, opt?: any) {
    return this.courseModel.findOne(courseByIdLOrSlugQuery(courseIdOrSlug), opt);
  }

  public findChapterWithConditions(
    conditions: Record<string, any>,
    fields: Record<string, number>,
  ) {
    return this.chapterModel.findOne(conditions, fields);
  }

  public findCourseByIdOrSlugWithConditions(conditions: any, fields: Record<string, number>) {
    return this.courseModel.find(conditions, fields);
  }

  public findByIdAndUpdateProfile(userId: string, dataToUpdate: Object, isNew: boolean) {
    return this.profileModel.findByIdAndUpdate(userId, dataToUpdate, {
      new: isNew,
    });
  }

  public aggregateAvarageProgress(course: ICourse) {
    return this.profileModel.aggregate([
      { $unwind: '$progress' },
      { $match: { 'progress.course_id': _.get(course, '_id') } },
      { $match: { 'progress.columnProgress.includeInAggregations': true } },
      {
        $project: {
          'progress.columnProgress': 1,
          'progress.course_id': 1,
          _id: 0,
        },
      },
      { $unwind: '$progress.columnProgress' },
      {
        $group: {
          _id: { course: '$progress.course_id', column: '$progress.columnProgress.columnId' },
          avgUnderstood: { $avg: '$progress.columnProgress.progressUnderstood' },

          avgProcessed: { $avg: '$progress.columnProgress.progressProcessed' },
        },
      },
    ]);
  }

  public updateClassAvarage(courseId: string, classAverageProgress: ClassAverageProgress) {
    return this.courseModel.findOneAndUpdate(
      { _id: courseId },
      { $set: { 'stats.classAverageProgress': classAverageProgress } },
    );
  }

  public async getProfilebyUserId(userId: string) {
    return await this.profileModel.findOne({ user_id: userId });
  }

  public async updateProfile(profile: IAuth0User, updates: object) {
    return this.profileModel.findOneAndUpdate(
      { user_id: profile.user_id },
      { ...updates },
      { strict: false },
    );
  }

  public async getProfileCourses(profile: IAuth0User) {
    const courseModels = await this.courseModel.find(
      { 'university.id': profile.mainUniversity.id },
      {
        'columns._chapters': 0,
        'columns.chapters': 0,
      },
    );
    return _.map(courseModels, c => c.toObject());
  }

  public async getCoursesByIds(courseIds: string[]) {
    const courseModels = await this.courseModel.find(courseIds ? { _id: { $in: courseIds } } : {}, {
      _id: 1,
      title: 1,
      university: 1,
      externalId: 1,
      url_slug: 1,
      price: 1,
      status: 1,
      hasPreview: 1,
      updatedAt: 1,
      stats: 1,
    });
    return _.map(courseModels, c => c.toObject());
  }

  public async createAnonymousUser(user: IAuth0User | string, anonymousId?: string) {
    const userId = _.get<string>(user, 'user_id', user);
    const lastLogin: string = _.get<string>(user, 'last_login', null);
    const existingProfile = await this.profileModel.findOne({
      $or: [{ user_id: userId }, { anonymousId: { $eq: anonymousId, $ne: null }, user_id: null }],
    });
    const doc = existingProfile || new this.profileModel({ user_id: userId, lastLogin });
    doc.anonymousId = userId && userId.indexOf(ANONYMOUS_PREFIX) < 0 ? null : anonymousId;
    await doc.save();
    return doc;
  }

  public async setMainUniversity(user: IAuth0User, university: any) {
    const userId = user.user_id;
    const profile = await this.profileModel.findOne({ user_id: userId });

    profile.mainUniversity = {
      id: university.id,
      url_slug: university.url_slug,
      name: university.name,
    } as any;
    profile.anonymousId = '123';
    return profile.save();
  }
}
