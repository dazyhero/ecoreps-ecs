import { Request } from 'express';
import { ProfileService } from '../profile.service';
import { DirectusClient } from '../../../shared/services/directus.client';
import { ProfileRespository } from '../../repository/profile.repository';
import { Model } from 'mongoose';
import {BadRequestException, NotFoundException} from '@nestjs/common';

import { IAuth0User, ICourse, IChapter } from '../../../shared/types/user.types';

jest.mock('../../repository/profile.repository');
jest.mock('../../../shared/services/directus.client');
jest.mock('jsonwebtoken', () => ({
    sign: jest.fn().mockReturnValue(Promise.resolve('test_token')),
    decode: jest.fn().mockReturnValue(Promise.resolve('test_token')),
}));
describe('ProfileService', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('getProfileForAuth0Id should return anonymous profile if existing not found', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.getProfileForAuth0Id('test_id');
        expect(profileRepositoryMock.getProfilebyUserId).toHaveBeenCalled();
        expect(profileRepositoryMock.createAnonymousUser).toHaveBeenCalled();
    });

    test('getProfileForAuth0Id should return existing profile', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getProfilebyUserId = jest.fn(() => Promise.resolve({ userId: 'test_user' } as IAuth0User));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        const user = await instance.getProfileForAuth0Id('test_id');
        expect(profileRepositoryMock.getProfilebyUserId).toHaveBeenCalled();
        expect(profileRepositoryMock.createAnonymousUser).not.toHaveBeenCalled();
        expect(user).toEqual({ userId: 'test_user' });
    });

    test('getMyProfile should return existing profile', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.getMyProfile('test_user');
        expect(profileRepositoryMock.getProfilebyUserId).toHaveBeenCalled();
    });

    test('getAvailableCourses should return profile courses if profile exists', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getProfilebyUserId = jest.fn(() => Promise.resolve({
            userId: 'test_user',
            mainUniversity: { name: 'test_university' },
            bookmarkedCourses: [{ _id: 'test_1' }, { _id: 'test_2' }],
        } as IAuth0User));
        profileRepositoryMock.getCoursesByIds = jest.fn(() => Promise.resolve( [
            { _id: 'test_1', status: 'published', stats: { chapterCount: '1' } },
            { _id: 'test_2', status: 'draft', stats: { chapterCount: '1' } },
        ]));
        profileRepositoryMock.getProfileCourses = jest.fn(() => Promise.resolve( [{ status: 'test_status' }]));
        profileRepositoryMock.updateProfile = jest.fn(() => Promise.resolve({
            userId: 'test_user',
            mainUniversity: { name: 'test_university' },
            bookmarkedCourses: [{ _id: 'test_1' }, { _id: 'test_2' }],
        } as IAuth0User));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.getAvailableCourses('test_user', { userId: 'test_user', app_metadata: {} } as IAuth0User, false);
        expect(profileRepositoryMock.getProfilebyUserId).toHaveBeenCalled();
        expect(profileRepositoryMock.updateProfile).toHaveBeenCalledWith({
            userId: 'test_user',
            mainUniversity: { name: 'test_university' },
            bookmarkedCourses: [{ _id: 'test_1' }, { _id: 'test_2' }],
        }, { bookmarkedCourses: [
                { _id: 'test_1', status: 'published', stats: { chapterCount: '1' } },
            ] });
        expect(profileRepositoryMock.getCoursesByIds).toHaveBeenCalled();
        expect(profileRepositoryMock.getProfileCourses).toHaveBeenCalled();
    });

    test('getAvailableCourses should throw error if profile do not have mainUniversity', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getProfilebyUserId = jest.fn(() => Promise.resolve({ userId: 'test_user' } as IAuth0User));
        profileRepositoryMock.getCoursesByIds = jest.fn(() => Promise.resolve( [{ status: 'test_status' }]));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        return expect(instance.getAvailableCourses('test_user', { userId: 'test_user', app_metadata: {} } as IAuth0User, false))
            .rejects.toBeInstanceOf(BadRequestException);
    });

    test('profileResponse should build profile response with isPurchased flag', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        const response = await instance.profileResponse({
            app_metadata: {
                purchasedCourses: ['1', '2'],
            },
        }, {
            bookmarkedCourses: [{
                _id: '1',
            }, {
                _id: '3',
            }],
        }, false);
        expect(response).toEqual({
            bookmarkedCourses: [{
                _id: '1', isPurchased: true,
            }, {
                _id: '3', isPurchased: false,
            }],
        });
    });

    test('syncProfileWithCourses should update profile with bookmarked courses', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getCoursesByIds = jest.fn(() => Promise.resolve([
            {
                status: 'published',
                stats: {
                    chapterCount: '1',
                },
            }, {
                status: 'draft',
                stats: {
                    chapterCount: '1',
                },
            }]));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.syncProfileWithCourses({ userId: 'test_user' } as IAuth0User, false);
        expect(profileRepositoryMock.getCoursesByIds).toHaveBeenCalled();
        expect(profileRepositoryMock.updateProfile).toHaveBeenCalledWith({ userId: 'test_user' }, { bookmarkedCourses: [{
                status: 'published',
                stats: {
                    chapterCount: '1',
                },
            }]});
    });

    test('syncProfileWithCourses should not update profile if there is no bookmarkedCourses on profile', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getCoursesByIds = jest.fn(() => Promise.resolve([]));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.syncProfileWithCourses({ userId: 'test_user', bookmarkedCourses: [] } as IAuth0User, false);
        expect(profileRepositoryMock.getCoursesByIds).toHaveBeenCalled();
        expect(profileRepositoryMock.updateProfile).not.toHaveBeenCalled();
    });

    test('syncProfileWithCourses should not update profile if there is no published courses', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getCoursesByIds = jest.fn(() => Promise.resolve([{ _id: 'test_1', status: 'draft', stats: { chapterCount: '1' } }]));
        const instance = new ProfileService(directusMock, profileRepositoryMock);

        await instance.syncProfileWithCourses({ userId: 'test_user', bookmarkedCourses: [{ _id: 'test_1' }] } as IAuth0User, false);
        expect(profileRepositoryMock.getCoursesByIds).toHaveBeenCalled();
        expect(profileRepositoryMock.updateProfile)
            .toHaveBeenCalledWith({ userId: 'test_user', bookmarkedCourses: [{ _id: 'test_1' }]}, { bookmarkedCourses: [] });
    });

    test('getOrCreateAnonymous should return token', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        profileRepositoryMock.getProfilebyUserId = jest.fn(
            () => Promise.resolve({ userId: 'test_user', toObject: () => ({ userId: 'test_user' }) } as IAuth0User),
        );

        const instance = new ProfileService(directusMock, profileRepositoryMock);

        const response = await instance.getOrCreateAnonymous();
        expect(response).toEqual({ token: 'test_token' });
    });

    test('setMainUniversity throw error if universityIdOrSlug not passed', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        const instance = new ProfileService(directusMock, profileRepositoryMock);
        return expect(instance.setMainUniversity('', { userId: 'test_user', app_metadata: {} } as IAuth0User, false))
            .rejects.toBeInstanceOf(NotFoundException);
    });

    test('setMainUniversity return no university found if there is no univerity found in directud', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        const instance = new ProfileService(directusMock, profileRepositoryMock);
        const response = await instance.setMainUniversity('test_university', { userId: 'test_user', app_metadata: {} } as IAuth0User, false);
        expect(response).toEqual('no university found');
    });

    test('setMainUniversity should set main university', async () => {
        const req = {} as Request;
        const directusMock = new DirectusClient(req);
        const profileRepositoryMock = new ProfileRespository(
            {} as Model<IAuth0User>,
            {} as Model<ICourse>,
            {} as Model<IChapter>,
        );
        directusMock.getItem = jest.fn(
            () => Promise.resolve({ id: 'test_university' } as any),
        );
        profileRepositoryMock.setMainUniversity = jest.fn(
            () => Promise.resolve({ userId: 'test_user', toObject: () => ({ userId: 'test_user' }) } as IAuth0User),
            );

        const instance = new ProfileService(directusMock, profileRepositoryMock);
        await instance.setMainUniversity('test_university', { userId: 'test_user' } as IAuth0User, false);
        expect(profileRepositoryMock.setMainUniversity).toHaveBeenCalledWith({ userId: 'test_user' }, { id: 'test_university' });
    });
});
