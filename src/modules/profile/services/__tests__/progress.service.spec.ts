import { Model } from 'mongoose';
import { Request } from 'express';
import DirectusClient from '../../../shared/services/directus.client';
import { IAuth0User, IChapter, ICourse } from '../../../shared/types/user.types';
import { ProfileRespository } from '../../repository/profile.repository';
import { ProgressService } from '../progress.service';
jest.mock('../../../shared/services/directus.client');
jest.mock('../../repository/profile.repository');

const req = {} as Request;
const directusMock = new DirectusClient(req);
const profileRepositoryMock = new ProfileRespository(
  {} as Model<IAuth0User>,
  {} as Model<ICourse>,
  {} as Model<IChapter>,
);
const instance = new ProgressService(directusMock, profileRepositoryMock);

describe('ProgressService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test('it should write avarage progress', async () => {
    const avgMock = {
      courseid: {
        progressUnderstood: 100,
        progressProcessed: 100,
      },
    };

    await instance.writeAverageClassProgress('courseid', avgMock);
    expect(profileRepositoryMock.updateClassAvarage).toHaveBeenCalled();
  });

  test('it should return null for my progress if no profile is provided', async () => {
    jest
      .spyOn(instance, 'getProfileForAuth0Id')
      .mockImplementationOnce(() => Promise.resolve({ toObject: () => {} }));
    const res = await instance.myProgress({ user_id: 'asdf' }, 'test', 'test');
    expect(res).toBeNull();
  });

  test('it should call get progress for no course for my progress if no courseId is provided', async () => {
    jest.spyOn(instance, 'getProfileForAuth0Id').mockImplementationOnce(() =>
      Promise.resolve({
        toObject: () => ({
          progress: [{ course_id: 'test', progressTotal: 100, progressProcessedTotal: 100 }],
        }),
      }),
    );
    const res = await instance.myProgress({ user_id: 'asdf' }, null, 'test');
    expect(res).toEqual([
      {
        course_id: 'test',
        progress: 100,
        progressProcessed: 100,
      },
    ]);
  });

  test('it should crete empty progress my progress if no profile progress found', async () => {
    jest.spyOn(instance, 'getProfileForAuth0Id').mockImplementationOnce(() =>
      Promise.resolve({
        toObject: () => ({
          progress: [{ course_id: 'test', progressTotal: 100, progressProcessedTotal: 100 }],
        }),
      }),
    );
    jest.spyOn(profileRepositoryMock, 'findByCourseOrSlug').mockImplementationOnce(() => ({
      _id: 'test1',
    }));
    const res = await instance.myProgress({ user_id: 'asdf' }, 'test', null);
    expect(res).toEqual({
      course_id: 'test1',
      columnProgress: [],
      progressTotal: 0,
      progressProcessedTotal: 0,
    });
  });

  test('it should get progress my progress if no chapterId is provided', async () => {
    jest.spyOn(instance, 'getProfileForAuth0Id').mockImplementationOnce(() =>
      Promise.resolve({
        toObject: () => ({
          progress: [
            {
              course_id: 'test',
              columnProgress: [
                {
                  id: 'test1',
                  progressTotal: 100,
                  progressProcessedTotal: 100,
                },
              ],
              progressTotal: 100,
              progressProcessedTotal: 100,
            },
          ],
        }),
      }),
    );
    jest.spyOn(profileRepositoryMock, 'findByCourseOrSlug').mockImplementationOnce(() => ({
      _id: 'test',
      columns: [
        {
          id: 'test',
          stats: {
            progressAverageProcessed: 100,
            progressAverageUnderstood: 100,
            classAverageProgress: 100,
          },
        },
      ],
      toObject: () => ({
        _id: 'test',
        stats: {
          classAverageProgress: null,
        },
        columns: [
          {
            id: 'test',
            stats: {
              progressAverageProcessed: 100,
              progressAverageUnderstood: 100,
              classAverageProgress: 100,
            },
          },
        ],
      }),
    }));
    const res = await instance.myProgress({ user_id: 'asdf' }, 'test', null);
    expect(res).toEqual({
      course_id: 'test',
      columnProgress: [
        {
          classAverage: {},
          id: 'test1',
          progressTotal: 100,
          progressProcessedTotal: 100,
        },
      ],
      progressTotal: 100,
      progressProcessed: 100,
    });
  });

  test('it should return null for progress if no profile is provided', async () => {
    const res = await instance.getProgress(null);
    expect(res).toBeNull();
  });

  test('it should return progress', async () => {
    jest.spyOn(profileRepositoryMock, 'getByUserId').mockImplementation(() =>
      Promise.resolve({
        progress: [{ course_id: 'test', progressTotal: 100, progressProcessedTotal: 100 }],
      }),
    );
    const res = await instance.getProgress('test');
    expect(res).toEqual([
      {
        course_id: 'test',
        progress: 100,
        progressProcessed: 100,
      },
    ]);
  });
});
