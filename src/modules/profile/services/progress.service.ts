import _ from 'lodash';
import { Document, Model } from 'mongoose';
import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { IAuth0User, ICourse, IChapter } from '../../shared/types/user.types';

import { DirectusClient } from '../../shared/services/directus.client';
import { FilterQuery } from 'mongodb';
import { notFound } from 'boom';
import { ProfileService } from './profile.service';
import { ProfileRespository } from '../repository/profile.repository';

import {
  IColumn,
  courseByIdLOrSlugQuery,
  isValidObjectId,
  ProgressState,
  ProgressType,
  ChapterTypeInfo,
  IProgress,
  IColumnProgress,
  IProgressChapterEntry,
} from '../../../../shared/src';
import IProfile from '../../../../shared/src/model/profile';

type ColumnsProgressItemsNumbers = Record<string, number>;

type ClassAverageProgress = {
  [id: string]: {
    progressUnderstood: number;
    progressProcessed: number;
  };
};

type ColumnsProgressMeta = {
  totalProgressItems: number;
  progressItems: ColumnsProgressItemsNumbers;
};

type MongoQuery<T extends Document> = {
  idOrSlug: string;
  fields: Record<string, number>;
  conditions?: FilterQuery<Model<T>>;
};

@Injectable()
export class ProgressService extends ProfileService {
  constructor(directusClient: DirectusClient, profileRespository: ProfileRespository) {
    super(directusClient, profileRespository);
  }

  private getClassFromCourse(courseClass: IAuth0User[], courseId: string): IProgress[] {
    return courseClass
      .map(profile => {
        return profile.progress.find(
          progress => progress.course_id && progress.course_id.toString() === courseId,
        );
      })
      .filter(Boolean);
  }

  private calculateClassProgress(
    classProgress: IProgress[],
    courseClass: IAuth0User[],
  ): ClassAverageProgress {
    const columnProgressSum = {};
    for (const { columnProgress } of classProgress) {
      for (const column of columnProgress) {
        const currentColumnSum = columnProgressSum[column.columnId];
        if (currentColumnSum) {
          columnProgressSum[column.columnId].progressUnderstood += column.progressUnderstood;
          columnProgressSum[column.columnId].progressProcessed += column.progressProcessed;
        } else {
          columnProgressSum[column.columnId] = {
            progressUnderstood: column.progressUnderstood,
            progressProcessed: column.progressProcessed,
          };
        }
      }
    }

    for (const column in columnProgressSum) {
      for (const progress in columnProgressSum[column]) {
        columnProgressSum[column][progress] /= courseClass.length;
      }
    }

    return columnProgressSum;
  }

  private async getClassProgress(courseId: string): Promise<ClassAverageProgress> {
    const courseClass = await this.profileRepository.getPurchasedCourses(courseId);
    const classProgress = this.getClassFromCourse(courseClass, courseId);

    const columnProgressSum = this.calculateClassProgress(classProgress, courseClass);

    return columnProgressSum;
  }

  async writeAverageClassProgress(
    courseId: string,
    classAverageProgress: ClassAverageProgress,
  ): Promise<ICourse> {
    return this.profileRepository.updateClassAvarage(courseId, classAverageProgress);
  }

  private getMyProgressForNoCourse(profile: IAuth0User) {
    return profile.progress.map(({ course_id, progressTotal, progressProcessedTotal }) => ({
      course_id,
      progress: progressTotal,
      progressProcessed: progressProcessedTotal,
    }));
  }

  private getMyProgressForNoChapter(profileProgress: IProgress, course: ICourse) {
    const columnProgress = profileProgress.columnProgress.map(column => {
      const others = course.columns.find(col => (col as any).id.toString() === column.columnId);
      if (
        others &&
        others.stats &&
        others.stats.progressAverageProcessed &&
        others.stats.progressAverageUnderstood
      ) {
        column.progressUnderstoodOthers = others.stats.progressAverageUnderstood;
        column.progressProcessedOthers = others.stats.progressAverageProcessed;
        return column;
      }
      return {
        ...column,
        classAverage: course.toObject().stats['classAverageProgress']
          ? course.toObject().stats['classAverageProgress'][column.columnId.toString()]
          : {},
      };
    });

    const result = {
      course_id: profileProgress.course_id,
      columnProgress,
      progressTotal: profileProgress.progressTotal,
      progressProcessed: profileProgress.progressProcessedTotal,
    };

    return result;
  }

  public async myProgress(user: IAuth0User, courseIdOrSlug: string, chapterIdOrSlug: string) {
    let profile = await this.getProfileForAuth0Id(user['user_id']);
    profile = profile.toObject();

    if (!profile || !profile.progress) {
      return null;
    }

    if (!courseIdOrSlug) {
      return this.getMyProgressForNoCourse(profile);
    }

    const course = await this.profileRepository.findByCourseOrSlug(courseIdOrSlug, {
      _id: 1,
      stats: 1,
      'columns.id': 1,
      'columns.stats': 1,
    });
    const courseId = course._id;

    if (!courseId) {
      throw new BadRequestException(`Course with id(slug) ${courseId} not found`);
    }

    const profileProgress = profile.progress.find(
      ({ course_id }) => course_id.toString() === courseId.toString(),
    );

    if (!profileProgress) {
      return {
        course_id: courseId,
        columnProgress: [],
        progressTotal: 0,
        progressProcessedTotal: 0,
      };
    }

    if (!chapterIdOrSlug) {
      return this.getMyProgressForNoChapter(profileProgress, course);
    }

    const chapter = await this.getChapter(chapterIdOrSlug, {
      url_slug: 1,
      columnId: 1,
      'sections._id': 1,
    });

    if (!chapter) {
      throw new BadRequestException('No chapter found');
    }

    const columnProgress = profileProgress.columnProgress.find(
      ({ columnId }) => columnId.toString() === chapter.columnId.toString(),
    );

    if (!columnProgress) {
      return { state: 0 };
    }

    const chapterProgress = (columnProgress.chapterProgress || []).find(
      ({ chapter: cp }) => cp.toString() === chapter._id.toString(),
    );

    if (!chapterProgress) {
      return {
        chapter: chapter._id,
        state: 0,
      };
    }

    return chapterProgress;
  }

  public async resetProgress(userData: IAuth0User, courseId: string, columnId: string) {
    const profile = await this.getProfileForAuth0Id(userData['user_id']);
    const [{ _id, columns }] = await this.getCourses({
      idOrSlug: courseId,
      fields: {
        'columns.id': 1,
        'columns._chapters': 1,
        'columns.stats': 1,
        'columns.progress_tracking': 1,
      },
    });

    //TODO see why schema doesn't see the id field on IColumnProgress
    const columnsToReset = columnId
      ? (columns as any).filter(({ id }) => id.toString() === columnId)
      : columns;

    const columnIds = columnsToReset.map(({ id }) => id);

    const progress = profile.progress.find(
      ({ course_id }) => course_id.toString() === _id.toString(),
    );

    progress.columnProgress = progress.columnProgress.filter(
      ({ columnId }) => !columnIds.includes(columnId),
    );

    await profile.save();

    this.recomputeProgress(profile, { _id, columns }, userData);

    return (
      profile.progress.find(({ course_id }) => course_id.toString() === _id.toString()) || null
    );
  }

  public async getProgress(userId: string): Promise<any> {
    const profile = await this.profileRepository.getByUserId(userId);
    if (!profile || !profile.progress) {
      return null;
    }

    return profile.progress.map(({ course_id, progressTotal, progressProcessedTotal }) => ({
      course_id,
      progress: progressTotal,
      progressProcessed: progressProcessedTotal,
    }));
  }

  private getProfileProgress(
    profile: IAuth0User,
    courseId: string,
    unset: boolean,
    columnId: string,
  ): { progress: IProgress; pushProgress: boolean } {
    let progress = profile.progress.find(p => p.course_id.toString() === courseId.toString());
    let pushProgress = false;

    if (!progress) {
      if (unset) {
        return null;
      }
      pushProgress = true;
      progress = {
        course_id: courseId,
        columnProgress: [],
        progressTotal: 0,
        progressProcessedTotal: 0,
      };
    }

    const hasColumn = progress.columnProgress.find(c => c.columnId === columnId);

    if ((!progress.columnProgress || !hasColumn) && columnId != null) {
      progress.columnProgress.push({
        columnId: columnId,
        chapterProgress: [],
        progressProcessed: 0,
        progressUnderstood: 0,
      });
    }

    return { progress, pushProgress };
  }

  private getProgressDivider(columns: IColumn[]): number {
    let divider = 0;
    for (const column of columns) {
      const chapterInfo = new ChapterTypeInfo(column);
      const progressType = chapterInfo.progressType;

      if (progressType === ProgressType.byChapter) {
        divider += this.getColumnChaptersCount(column);
      } else if (progressType === ProgressType.bySection) {
        divider += column.stats.sectionCount;
      }
    }

    return Math.max(divider, 1);
  }

  private async saveAverageForCourse(courseId: string) {
    let course = await this.profileRepository.findByCourseOrSlug(courseId);

    let averages = await this.profileRepository.aggregateAvarageProgress(course);
    for (let a of averages) {
      if (a._id.course !== courseId) {
        continue;
      }
      const col = course.columns.find(col => _.get(col, 'id') === _.get(a, '_id.column'));
      if (!col) {
        continue;
      }
      col.stats.progressAverageUnderstood = a.avgUnderstood;
      col.stats.progressAverageProcessed = a.avgProcessed;
    }

    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    const averageProgress = (course['stats'] as any).toObject()['classAverageProgress'];
    if (!!averageProgress) {
      (course.stats as any).progressAverageUnderstood =
        Object.keys(averageProgress)
          .map(columnKey => averageProgress[columnKey].progressUnderstood || 0)
          .reduce(reducer, 0) / Math.max(Object.keys(averageProgress).length, 1);
      (course.stats as any).progressAverageProcessed =
        Object.keys(averageProgress)
          .map(columnKey => averageProgress[columnKey].progressProcessed || 0)
          .reduce(reducer, 0) / Math.max(Object.keys(averageProgress).length, 1);
    }

    await course.save();
  }

  private async getCourses(query: MongoQuery<ICourse>): Promise<ICourse[]> {
    const { idOrSlug, fields, conditions = {} } = query;
    const course = await this.profileRepository.findCourseByIdOrSlugWithConditions(
      {
        $and: [courseByIdLOrSlugQuery(idOrSlug), conditions as object],
      },
      fields,
    );

    if (course.length < 1) {
      throw new BadRequestException(`Course with id(slug) ${idOrSlug} not found`);
    }

    return course;
  }

  private async getCourse(query: MongoQuery<ICourse>) {
    const { idOrSlug, fields } = query;
    const course = await this.profileRepository.findByCourseOrSlug(idOrSlug, fields);

    if (!course) {
      throw new BadRequestException(`Course with id(slug) ${idOrSlug} not found`);
    }

    return course;
  }

  private getColumn(course, columnId: string) {
    const column = course.columns.find(column => columnId === column.id);
    if (!column) {
      throw new BadRequestException(`Column with id(slug) ${columnId} not found`);
    }

    return column;
  }

  private getColumnChaptersCount(column): number {
    const validChapters = column._chapters.filter(chapter => chapter.hasSections);
    return validChapters.length;
  }

  private getColumnsProgressMeta(columns): ColumnsProgressMeta {
    const columnsProgressMeta: ColumnsProgressMeta = {
      totalProgressItems: 0,
      progressItems: {},
    };

    for (const column of columns) {
      const chapterInfo = new ChapterTypeInfo(column);
      const progressType = chapterInfo.progressType;

      if (progressType === ProgressType.byChapter) {
        const chapterNumber = this.getColumnChaptersCount(column);

        columnsProgressMeta.totalProgressItems += chapterNumber;
        columnsProgressMeta.progressItems[column.id] = chapterNumber;
      } else if (progressType === ProgressType.bySection) {
        const sectionNumber = column.stats.sectionCount;

        columnsProgressMeta.totalProgressItems += sectionNumber;
        columnsProgressMeta.progressItems[column.id] = sectionNumber;
      }
    }

    return columnsProgressMeta;
  }

  private getChapterEntry(
    columnProgress: IColumnProgress,
    chapterId: string,
    unset: boolean,
    shouldSetChapterState: boolean,
    state: ProgressState,
  ): { pushChapter: boolean; chapterEntry: IProgressChapterEntry } {
    if (!columnProgress) {
      return null;
    }
    let chapterEntry = columnProgress.chapterProgress.find(c => c.chapter.toString() === chapterId);
    let pushChapter = false;

    if (!chapterEntry) {
      if (unset) {
        return null;
      }
      pushChapter = true;
      chapterEntry = {
        chapter: chapterId,
        sections: [],
        state: shouldSetChapterState ? state : ProgressState.undefined,
      };
    } else if (shouldSetChapterState) {
      chapterEntry.state = state;
    }

    return { pushChapter, chapterEntry };
  }

  private async getChapter(chapterId: string, fields: Record<string, number>): Promise<IChapter> {
    const chapter = await this.profileRepository.findChapterWithConditions(
      isValidObjectId(chapterId) ? { _id: chapterId } : { url_slug: chapterId },
      fields,
    );

    if (!chapter) {
      throw new BadRequestException(`Chapter with id(slug) ${chapterId} not found`);
    }

    return chapter;
  }

  private getSection(
    chapter: IChapter,
    chapterEntry: IProgressChapterEntry,
    sectionToFindId: string,
    unset: boolean,
    state: ProgressState,
  ) {
    let section = chapter.sections.find(s => s._id.toString() === sectionToFindId);

    if (!section) {
      throw new BadRequestException(`Section ${sectionToFindId}`);
    }

    const sectionId = section._id;

    if (chapterEntry.sections.map(sectionEntry => sectionEntry.section).indexOf(sectionId) === -1) {
      if (unset) {
        return null;
      }
      chapterEntry.sections.push({
        state: state,
        section: sectionId,
      });
    } else if (unset) {
      chapterEntry.sections = chapterEntry.sections.filter(s => s.section !== sectionId);
    } else {
      let entry = chapterEntry.sections.find(s => s.section.toString() === sectionId.toString());
      entry.state = state;
    }
  }

  public async getProfileForUser(userId, anonymousId?) {
    if (!userId) {
      throw new NotFoundException({
        message: 'User not found',
      });
    }
    return this.getProfileForAuth0Id(userId, anonymousId);
  }

  public async complete(
    body,
    userData: IAuth0User,
    courseIdOrSlug: string,
    isAdmin: boolean,
    anonymous?: string,
  ) {
    const { chapterId: existingChapterId, state, unset } = body;
    const shouldSetChapterState = !body.sectionId;
    let [course, chapter, profile] = await Promise.all([
      this.getCourse({ idOrSlug: courseIdOrSlug, fields: { _id: 1, columns: 1 } }),
      this.getChapter(existingChapterId, { url_slug: 1, columnId: 1, 'sections._id': 1 }),
      this.getProfileForUser(userData['user_id'], anonymous),
    ]);

    const column = this.getColumn(course, chapter.columnId);
    const { progress, pushProgress } = this.getProfileProgress(
      profile,
      course._id,
      unset,
      column.id,
    );
    const emptyProgress: IColumnProgress = {
      columnId: column.id.toString(),
      progressUnderstood: 0,
      progressProcessed: 0,
      chapterProgress: [],
    };
    const columnProgress =
      progress.columnProgress
        .filter(Boolean)
        .find(c => (c.columnId ? c.columnId.toString() === column.id.toString() : false)) ||
      emptyProgress;

    const { pushChapter, chapterEntry } = this.getChapterEntry(
      columnProgress,
      chapter.id,
      unset,
      shouldSetChapterState,
      state,
    );

    if (body.sectionId) {
      this.getSection(chapter, chapterEntry, body.sectionId, unset, state);
    } else if (unset) {
      columnProgress.chapterProgress = columnProgress.chapterProgress.filter(
        c => c.chapter !== chapter._id,
      );
    }

    if (pushProgress) {
      profile.progress.push(progress);
    }

    if (pushChapter) {
      if (!columnProgress.chapterProgress) {
        columnProgress.chapterProgress = [];
      }
      columnProgress.chapterProgress.push(chapterEntry);
    }

    profile.progress = profile.progress.map(oldProgress =>
      oldProgress.course_id === progress.course_id ? progress : oldProgress,
    );

    profile.markModified('progress');
    const newprofile = await profile.save();

    //if (pushProgress) {
    await this.recomputeProgress(newprofile, course, userData);
    //}

    //update profile progress
    const finalProgress = newprofile.progress.find(
      p => p.course_id.toString() === course._id.toString(),
    );

    if (!finalProgress) {
      return null;
    }

    const hasPurchasedCourses = this.hasPurchasedCourses(
      course._id,
      userData['app_metadata'].purchasedCourses || [],
      isAdmin,
    );

    const newProgress = await this.computeProgress(
      await this.saveProfile(newprofile, isAdmin),
      course._id,
      column,
      hasPurchasedCourses,
    );

    //Log.log("computed progress with: " + JSON.stringify(newprofile.progress) + " -- " + courseId + " and type" + chapter.columnId);
    columnProgress.progressProcessed = newProgress.progressProcessed;
    columnProgress.progressUnderstood = newProgress.progressUnderstood;

    //Log.log("ABOUT TO SET: " + JSON.stringify(ProfileController.calculateAverageProgress(finalProgress.columnProgress, validColCount)));
    const progressMeta = this.getColumnsProgressMeta(course.columns);

    finalProgress.progressTotal = ProgressService.calculateAverageColumnProgressForSingleUser(
      finalProgress.columnProgress,
      progressMeta.totalProgressItems,
      progressMeta.progressItems,
    ).progressUnderstood;

    finalProgress.progressProcessedTotal = ProgressService.calculateAverageColumnProgressForSingleUser(
      finalProgress.columnProgress,
      progressMeta.totalProgressItems,
      progressMeta.progressItems,
    ).progressProcessed;

    //Log.log("has written: ");
    //this can be performance optimized by checking when the last course sync happened
    newprofile.progress = newprofile.progress.map(progress =>
      progress.course_id === finalProgress.course_id ? finalProgress : progress,
    );

    await newprofile.save();

    newprofile.markModified('progress');

    await Promise.all([
      this.saveAverageForCourse(course._id),
      this.saveProfile(newprofile, isAdmin),
    ]);

    const classAverageProgress = await this.getClassProgress(course._id.toString());

    await this.writeAverageClassProgress(course._id.toString(), classAverageProgress);

    //this is not waited for on purpose !

    return chapterEntry || null;
  }

  public async recomputeProgress(profile, course, auth0User) {
    let progress = profile.progress.find(
      progress => progress.course_id.toString() === course._id.toString(),
    );
    if (!progress) {
      console.error('tried to recompute progress for non-existing course');
      return;
    }

    const auth0UserMeta = auth0User['app_metadata'];

    const auth0UserPurchasedCourses = auth0UserMeta && auth0UserMeta.purchasedCourses;
    const auth0UserIsAdmin = auth0UserMeta && auth0UserMeta.roles.includes('admin');
    let validColCount = 0;

    for (let col of course.columns) {
      let prog = await this.computeProgress(
        profile,
        course._id,
        col,
        this.hasPurchasedCourses(course._id, auth0UserPurchasedCourses, auth0UserIsAdmin),
      );

      let colProgress = progress.columnProgress.find(c => c.columnId.toString() == col.id);

      if (!colProgress) {
        progress.columnProgress.push({
          progressUnderstood: 0,
          progressProcessed: 0,
          chapterProgress: [],
          columnId: col.id,
        });
      }
      let colProgress2 = progress.columnProgress.find(c => c.columnId.toString() == col.id);
      colProgress2.progressProcessed = prog.progressProcessed;
      colProgress2.progressUnderstood = prog.progressUnderstood;
      colProgress2.includeInAggregations = this.hasPurchasedCourses(
        course._id,
        auth0UserPurchasedCourses,
        auth0UserIsAdmin,
      );
      if (col.stats.chapterCount > 0) {
        validColCount++;
      }
    }

    const progressMeta = this.getColumnsProgressMeta(course.columns);
    progress.progressTotal = ProgressService.calculateAverageColumnProgressForSingleUser(
      progress.columnProgress,
      progressMeta.totalProgressItems,
      progressMeta.progressItems,
    ).progressUnderstood;

    progress.progressProcessedTotal = ProgressService.calculateAverageColumnProgressForSingleUser(
      progress.columnProgress,
      progressMeta.totalProgressItems,
      progressMeta.progressItems,
    ).progressProcessed;

    return await this.saveProfile(profile, auth0UserIsAdmin);
  }

  private getColumnProgress(
    columnProgress: IColumnProgress,
    columnId: string,
    includeInAggregations: boolean,
    courseProgress,
  ) {
    if (!columnProgress) {
      columnProgress = {
        columnId: columnId,
        chapterProgress: [],
        progressProcessed: 0,
        progressUnderstood: 0,
        includeInAggregations,
      };
      courseProgress.columnProgress.push(columnProgress);
    } else {
      columnProgress.includeInAggregations = includeInAggregations;
    }
  }

  public async computeProgress(
    profile: IAuth0User,
    courseId: string,
    column,
    includeInAggregations: boolean,
  ) {
    let result = {
      progressProcessed: 0,
      progressUnderstood: 0,
    };

    let info = new ChapterTypeInfo(column);
    let relevantChapters = column._chapters.filter(c => c.hasSections);

    let courseProgress = profile.progress.find(p => p.course_id.toString() === courseId.toString());

    if (!courseProgress) {
      return result;
    }

    let columnProgress = courseProgress.columnProgress.find(c => c.columnId == column.id);

    if (!columnProgress) {
      return result;
    }
    this.getColumnProgress(columnProgress, column.id, includeInAggregations, courseProgress);

    let max = 0;
    if (info.progressType == ProgressType.byChapter) {
      max = relevantChapters.length;
    } else if (info.progressType == ProgressType.bySection) {
      max = column.stats.sectionCount;
    }
    if (max <= 0) {
      return result;
    }

    const chapterProgress = columnProgress.chapterProgress || [];

    let processedChapterIds = chapterProgress.map(c => c.chapter);
    let understoodChapterIds = chapterProgress
      .filter(cp => cp.state == ProgressState.fullyUnderstood)
      .map(c => c.chapter);

    let relevantChapterIds = relevantChapters.map(c => c._id.toString());
    let processed = relevantChapterIds.reduce((counter, chapterId) => {
      if (processedChapterIds.indexOf(chapterId) >= 0) {
        if (info.progressType == ProgressType.bySection) {
          let chapterEntry = chapterProgress.find(
            c => c.chapter.toString() == chapterId.toString(),
          );
          if (chapterEntry) {
            counter += chapterEntry.sections.length;
          }
        } else {
          counter += 1;
        }
      }
      return counter;
    }, 0);

    let completed = relevantChapterIds.reduce((counter, chapterId) => {
      if (processedChapterIds.indexOf(chapterId) >= 0) {
        if (info.progressType == ProgressType.bySection) {
          let chapterEntry = columnProgress.chapterProgress.find(
            c => c.chapter.toString() == chapterId.toString(),
          );
          if (chapterEntry) {
            for (let sec of chapterEntry.sections) {
              counter += sec.state === ProgressState.fullyUnderstood ? 1 : 0;
            }
          }
        } else if (understoodChapterIds.indexOf(chapterId) >= 0) {
          counter += 1;
        }
      }
      return counter;
    }, 0);

    result.progressProcessed = processed / max;
    result.progressUnderstood = completed / max;
    return result;
  }

  private static calculateAverageColumnProgressForSingleUser(
    columnProgress,
    maxNumberOfColumns,
    columnProgressItems?: ColumnsProgressItemsNumbers,
  ) {
    let average = {
      progressUnderstood: 0,
      progressProcessed: 0,
    };

    let colProgCount = 0;
    if (columnProgress.length == 0) {
      return {
        progressUnderstood: 0,
        progressProcessed: 0,
      };
    }

    for (let prog of columnProgress) {
      const progressItemsNumbers = columnProgressItems[prog.columnId];
      average = {
        progressUnderstood:
          average.progressUnderstood + prog.progressUnderstood * progressItemsNumbers,
        progressProcessed:
          average.progressProcessed + prog.progressProcessed * progressItemsNumbers,
      };
      colProgCount++;
    }

    if (colProgCount == 0) {
      throw new BadRequestException('invalid column number');
    }

    colProgCount =
      maxNumberOfColumns != null && maxNumberOfColumns > 0 ? maxNumberOfColumns : colProgCount;
    average = {
      progressUnderstood: average.progressUnderstood / colProgCount,
      progressProcessed: average.progressProcessed / colProgCount,
    };

    return average;
  }

  public async bookmarkCourse(courseId: string, user: IAuth0User, isAdmin: boolean) {
    const userId = _.get(user, 'user_id');
    const syncedProfile = await this.bookmarkCourseId(userId, courseId, isAdmin);
    return this.profileResponse(user, syncedProfile, isAdmin);
  }

  private async bookmarkCourseId(userId, courseId, isAdmin) {
    const user = await this.getProfileForAuth0Id(userId);
    let course: ICourse = await this.profileRepository.findByCourseOrSlug(courseId, {
      _id: 1,
      title: 1,
      status: 1,
      university: 1,
      externalId: 1,
      price: 1,
      url_slug: 1,
      hasPreview: 1,
    });
    if (!course) {
      throw notFound('course: ' + courseId);
    }
    if (
      _.chain(user)
        .get('bookmarkedCourses')
        .findIndex(c => _.get(c, '_id', null).toString() === _.get(course, '_id').toString()) >= 0
    ) {
      return user;
    } else {
      const newBookmark = _.chain(user)
        .get('bookmarkedCourses', [])
        .concat([course])
        .value();

      let dateToUpdate: any = { bookmarkedCourses: newBookmark };

      const { progress, pushProgress } = this.getProfileProgress(user, course._id, false, null);

      if (pushProgress) {
        let userProgress = user.progress;
        userProgress.push(progress);
        dateToUpdate.progress = userProgress;
      }

      const newProfile = await this.profileRepository.findByIdAndUpdateProfile(
        _.get(user, '_id'),
        dateToUpdate,
        true,
      );

      return await this.syncProfileWithCourses(newProfile, isAdmin);
    }
  }

  public async removeBookmarkedCourses(
    courseId: string,
    userId: string,
    isAdmin: boolean,
    anonymous?: string,
  ): Promise<any> {
    const profile: IAuth0User = await this.getProfileForUser(userId, anonymous);
    const course: ICourse = await this.profileRepository.findByCourseOrSlug(courseId);
    if (!course) {
      throw new NotFoundException({ message: 'Courses not found' });
    }
    const existingBookmarkedCourse = _.chain(profile)
      .get('bookmarkedCourses')
      .findIndex(bookmarkedCourse => {
        const bookMarkId = _.get(bookmarkedCourse, '_id', null).toString();
        const courseId = _.get(course, '_id').toString();
        return courseId === bookMarkId;
      })
      .value();
    if (existingBookmarkedCourse < 0) {
      return profile;
    } else {
      const synced = await this.syncProfileWithCourses(profile, isAdmin);
      const filteredBookmarkedCourses = _.chain(synced)
        .get('bookmarkedCourses')
        .filter(c => {
          const bookMarkId = _.get(c, '_id', null).toString();
          const courseId = _.get(course, '_id').toString();
          return courseId !== bookMarkId;
        })
        .value();
      return this.profileRepository.findByIdAndUpdateProfile(
        _.get(profile, '_id'),
        { $set: { bookmarkedCourses: filteredBookmarkedCourses } },
        true,
      );
    }
  }
}
