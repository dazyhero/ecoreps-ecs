import _ from 'lodash';
import { Types } from 'mongoose';
import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { IAuth0User, ICourse } from '../../shared/types/user.types';

import { DirectusClient } from '../../shared/services/directus.client';
import { ProfileRespository } from '../repository/profile.repository';
import * as jwt from 'jsonwebtoken';
import { ANONYMOUS_PREFIX, PublishState } from '../../../../shared/src';

@Injectable()
export class ProfileService {
  constructor(
    public directusClient: DirectusClient,
    public profileRepository: ProfileRespository,
  ) {}

  public async getProfileForAuth0Id(
    userId: string,
    anonymousId?: string,
    purchasedCourses?: string[],
  ): Promise<IAuth0User> {
    const existingProfile = await this.profileRepository.getProfilebyUserId(userId);
    const profile =
      existingProfile || (await this.profileRepository.createAnonymousUser(userId, anonymousId));

    if (purchasedCourses) {
      await this.syncPurchasedCourse(purchasedCourses, profile);
    }
    return profile;
  }

  private async syncPurchasedCourse(
    purchasedCourses: string[],
    profile: IAuth0User,
  ): Promise<IAuth0User> {
    return this.profileRepository.updateProfile(profile, { purchasedCourses });
  }

  public async getMyProfile(userId: string): Promise<any> {
    return await this.getProfileForAuth0Id(userId, null, null);
  }

  /**
   * Returns the courses of the users main university, available if they are bookmarked
   */
  public async getAvailableCourses(
    userId: string,
    userData: IAuth0User,
    isAdmin: boolean,
  ): Promise<any> {
    const profile = await this.getProfileForAuth0Id(
      userId,
      null,
      userData.app_metadata.purchasedCourses,
    );
    if (profile) {
      if (!profile.mainUniversity) {
        throw new BadRequestException({ error: 'missing main university' });
      }
      const syncedProfile = await this.syncProfileWithCourses(profile, isAdmin);
      const bookmarkedCourses = _.get(syncedProfile, 'bookmarkedCourses', []);

      const courses = await this.profileRepository.getProfileCourses(profile);

      return courses.map(course => {
        return {
          ...course,
          active: !!bookmarkedCourses.find(bookmarkedCourse => bookmarkedCourse._id === course._id),
        };
      });
    } else {
      return [];
    }
  }

  public profileResponse(userProfile, syncedProfile, isAdmin) {
    let profile: IAuth0User = syncedProfile;
    if (typeof profile.toObject === 'function') {
      profile = profile.toObject();
    }
    return {
      ...(profile || syncedProfile),
      bookmarkedCourses: _.chain(syncedProfile)
        .get('bookmarkedCourses', [{}])
        //@ts-ignore
        .map(course => {
          const isPurchased = this.hasPurchasedCourses(
            _.get(course, '_id'),
            _.get(userProfile, 'app_metadata.purchasedCourses', []),
            isAdmin,
          );
          return {
            ...course,
            isPurchased: isPurchased || _.get(course, 'price.price') > 0,
          };
        })
        .value(),
    };
  }

  public async syncProfileWithCourses(profile: IAuth0User, admin: boolean): Promise<IAuth0User> {
    let update = false;
    const updateCourses: ICourse[] = [];
    const bookmarkedIds = _.chain(profile)
      .get('bookmarkedCourses')
      .map(c => c._id)
      .value();
    const bookmarkedCourses = await this.memberCoursesForIds(admin ? null : bookmarkedIds);

    for (const courseModel of bookmarkedCourses) {
      if (courseModel) {
        if (
          _.get(courseModel, 'status').toString() === _.get(PublishState, 'published').toString() &&
          _.get(courseModel, 'stats.chapterCount') > 0
        ) {
          updateCourses.push(courseModel);
        }
        update = true;
      }

      if (!update) {
        return profile;
      }

      return await this.profileRepository.updateProfile(profile, {
        bookmarkedCourses: updateCourses,
      });
    }
  }

  private async memberCoursesForIds(courseIds: string[]) {
    return this.profileRepository.getCoursesByIds(courseIds);
  }

  public hasPurchasedCourses(courseId, purchasedCourses, isAdmin) {
    if (courseId === null || courseId === undefined) {
      return false;
    }
    return isAdmin || purchasedCourses.indexOf(courseId.toString()) >= 0;
  }

  public async saveProfile(profile, admin) {
    const synced = await this.syncProfileWithCourses(profile, admin);
    return await synced.save();
  }

  public async getOrCreateAnonymous(requestJwt: string = null): Promise<{ token: string }> {
    const existingJWT = jwt.decode(requestJwt);
    const anonymousId = _.get(existingJWT, ['anonymousId']);
    if (anonymousId) {
      return { token: await this.getAnonymous(anonymousId) };
    }
    const newAnonymousId = await this.setAnonymous();
    return { token: await this.getAnonymous(newAnonymousId) };
  }

  private async getAnonymous(anonymousId: string): Promise<string> {
    const profile = await this.profileRepository.getProfilebyUserId(anonymousId);
    if (profile) {
      const auth0FakeProfile = this.fromProfileToAuth0Object(profile.toObject());
      return jwt.sign(auth0FakeProfile, ANONYMOUS_PREFIX);
    } else {
      return null;
    }
  }

  private async setAnonymous(): Promise<string> {
    const anonymousId = Types.ObjectId().toString();
    const userId = `${ANONYMOUS_PREFIX}|${anonymousId}`;
    await this.profileRepository.createAnonymousUser(userId, anonymousId);
    return anonymousId;
  }

  private fromProfileToAuth0Object(profile: IAuth0User): any {
    return {
      ...profile,
      'https://ecoreps.de/roles': [],
      'https://ecoreps.de/purchased_courses': [],
      app_metadata: {
        roles: [],
      },
    };
  }

  public async setMainUniversity(universityIdOrSlug: string, user: IAuth0User, isAdmin: boolean) {
    if (!universityIdOrSlug) {
      throw new NotFoundException({ 'not found': universityIdOrSlug });
    }

    const university = await this.directusClient.getItem<any>('university', universityIdOrSlug);

    if (!university) {
      return 'no university found';
    }

    const profile = await this.profileRepository.setMainUniversity(user, university);
    return this.profileResponse(user, profile, isAdmin);
  }
}
