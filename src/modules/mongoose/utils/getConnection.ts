import mongoose from 'mongoose';

export default function getConnection() {
  const cache = {};
  return (db: string) => {    
    if (cache[db]) return cache[db];    
    const connection = mongoose.createConnection(db);
    cache[db] = connection;
    return connection;
  };
}
