import { get as GetConfig } from 'config';
import { DB_CONNECT_INJECTION } from '../constants/db';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Provider, Scope } from '@nestjs/common';
import getConnection from './utils/getConnection';

const dbURL = process.env.MONGO_URL || GetConfig('Service.Database.connectionString');

const dbs = {
  100: GetConfig('Service.Database.ecoreps'),
  200: GetConfig('Service.Database.studyprime'),
};

const connections = getConnection();

export const mongooseProviders: Provider<any> = {
  provide: DB_CONNECT_INJECTION,
  scope: Scope.REQUEST,
  useFactory: async (req: Request) => {
    const { clientid } = req.headers;
    const db = dbs[Number(clientid)];
    return connections(`${dbURL}/${db}`);
  },
  inject: [REQUEST],
};
