import { Module } from '@nestjs/common';
import { DB_CONNECT_INJECTION } from '../constants/db';
import { mongooseProviders } from './mongoose.provider';

@Module({
  providers: [mongooseProviders],
  exports: [DB_CONNECT_INJECTION, mongooseProviders],
})
export class MongooseModule {}
