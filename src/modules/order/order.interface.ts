import { Document } from 'mongoose';


export interface IVoucherValidationResponse {
    valid: boolean;
    discountInCents?: number;
    message?: string;
  }
  
  export interface IUsedVoucher extends Document{
    id: string;
    code: string;  
    date_redeemed: Date;
  }
  
  export interface IVoucher {
    id: string;
    title: string;
    discount_in_cents: string;
    single_use: Boolean;
    code: string;  
  }
  
  
  
  export enum VoucherErrorType {
    voucherIsUsed = "Der Gutschein wurde bereits eingelöst",
    codeDoesNotExist = "Gutscheincode ungültig",
  }
  
  export class VoucherError extends Error {
    constructor(m: VoucherErrorType) {
        super(m);
  
        // Set the prototype explicitly.
        Object.setPrototypeOf(this, VoucherError.prototype);
    }
  }