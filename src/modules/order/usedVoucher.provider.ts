import { DB_CONNECT_INJECTION, ORDER_MODEL, USEDVOUCHER_MODEL } from '../constants/db';
import mongoose, { Connection } from 'mongoose';

export const UsedVoucherSchema = new mongoose.Schema(
    {      
      id: { type: String, required: true, unique: true },      
      code: { type: String, required: true, unique: false },      
      date_redeemed: { type: Date, required: true, unique: false, default: Date() },      
    },
    {
      timestamps: true,
    },
  );

export const usedVoucherProvider = [
    {
      provide: USEDVOUCHER_MODEL,
      useFactory: (connection: Connection) => connection.model(USEDVOUCHER_MODEL, UsedVoucherSchema),
      inject: [DB_CONNECT_INJECTION],
    },
  ];
  