export function optional(prefix: string, value: string | number, suffix: string ='', newline: boolean = true): string {
    if (typeof value === 'number') {
        if(value && parseFloat(value.toString()) > 0) {
            value = value.toString()
        } else {
            return '';
        }
    }
    return (value) ? (prefix || '') + value + (suffix || '') + (newline ? '\n' : '') : ''
}

export function forEachItem<T> (items: T[], builder: (T) => string , newline: boolean = true): string {
    let result: string = "";
    for (let item of items) {
        result += builder(item) + (newline ? '\n' : '');
    }
    return result;
}

export function indent(n: number): string {
    const pixel = n * 10;
    return `<p style="padding:0; margin: 0;text-indent: ${pixel}px"/>`
}