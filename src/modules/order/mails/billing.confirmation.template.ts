import Order, { CourseOfferState, ISummaryResponse } from "../model/order"
import { forEachItem, indent, optional } from "./templateHelpers"



export function billingConfirmationTemplate(order: Order): string { 
return `
Hallo ${order.fullName},

Vielen Dank für deine Bestellung und herzlich Willkommen in unseren Kursen! :)

Deine Daten im Überblick:
${indent(1)} ${order.fullName}
${indent(1)}${ order.street } ${order.streetNumber}
${indent(1)}${ order.city } ${order.plz}
${indent(1)}${order.country}
${indent(1)}${optional("Mobil/Tel: ",order.phoneNumber)}

Deine Bestellung im Überblick

${ forEachItem(order.summaryResponse.courseOffers.filter(offer => offer.state == CourseOfferState.inCart), (offer => {
    return `    ${indent(1)} Kurs:  ${offer.title}
                ${indent(1)} Betrag: ${offer.price.value/100} ${order.summaryResponse.currency}
    ` })) } ${ forEachItem(order.summaryResponse.addonOffers.filter(offer => offer.state == CourseOfferState.inCart), (offer => {
    return `    ${indent(1)} Add-On: ${offer.title}
                ${indent(1)} Betrag: ${offer.price.value/100} ${order.summaryResponse.currency}
    ` })) }
${indent(1)} ${optional("Rabattcode: ", (order.summaryResponse.effectiveVouchers || []) [0])} ${indent(1)} ${optional("Rabatt: ", order.summaryResponse.totalVoucherDiscount/100, " "+order.summaryResponse.currency) } ${indent(1)} Gesamtbetrag: ${ order.summaryResponse.total.value/100 } ${ order.summaryResponse.currency }

Du hast die Bezahlung per Überweisung ausgewählt. Bitte überweise den oben aufgeführten Betrag in den nächsten Tagen an das folgende Konto:

${indent(1)} Zahlungsempfänger: Klausurvorbereitung
${indent(1)} IBAN: DE14 6905 0001 0026 4784 53
${indent(1)} BIC: SOLADES1KNZ
${indent(1)} Verwendungszweck: ${order._id}

Du hast bereits jetzt einen kompletten Zugang zu deinen Kursen und kannst direkt anfangen sie zu nutzen.

Falls du Fragen zum Kurs oder den Inhalten hast, kannst du dich jederzeit bei uns melden:

${ forEachItem(order.summaryResponse.courseOffers.filter(offer => offer.state == CourseOfferState.inCart), (offer => {
    return `    ${indent(1)} Kurs:  ${offer.title}
                ${indent(1)} Kontakt:  ${offer.contactDescription}             
    ` })) }
Viele Grüße und viel Erfolg beim Lernen
Studyprime-Team`
}

