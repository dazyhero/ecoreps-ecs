import { Min, Max, IsInt } from 'class-validator';
import { isMainThread } from 'worker_threads';


export class BasicOrderRequest {
    summary: ISummaryRequest;  
    expectedTotal: number; 
    phoneNumber?: string;
    billing: Address 
  }
  
  
  export class PaypalOrderRequest extends BasicOrderRequest {  
    
  }
  
  class Address {        
    @Max(3)
    fullName: string;          
  }
  class FullAddress {
    fullName: string;
    street: string;
    streetNumber: string;
    city: string;
    @IsInt()
    @Min(4)
    @Max(5)
    plz: string;
    @Min(4)
    @Max(5) 
    country: string;
  }
  
  export class OrderRequest extends BasicOrderRequest {
    billing: FullAddress
  }
  
  
  export enum CourseOfferState {
    offered = 0,
    inCart = 1,
    purchased = 2
  }
  
  export interface PriceInfo {
    originalValue?: number;  
    value: number,  
    savingInPercent?: number  
  }
  
  export interface IHasPriceInfo {
    price: PriceInfo;
  }
  
  export interface IHasOfferState {
    state: CourseOfferState;
  }
  
  export interface ICourseItem {
    id: string,
    title: string,
    product_category: string;
  }
  export interface IAddonOffer extends IHasPriceInfo, IHasOfferState {
    id: string,
    title: string,  
    description: string,  
  }
  
  export interface ISummaryResponse {
    currency: string;
    courseOffers:   
      (IHasOfferState & IHasPriceInfo & ICourseItem)[],
    addonOffers: IAddonOffer[]
    effectiveVouchers?: string[],
    totalVoucherDiscount: number,
    total: PriceInfo
  
  
  }
  
  export interface ISummaryRequest {
    course: string;  
    selectedCourseIds: string[];
    selectedAddonIds: string[];
    activeVoucher: string;  
  }
  
  export default class Order extends BasicOrderRequest {
    _id: any;
    course: string;
    userId: string;
    createdAt: number;
    status: OrderStatus;
    statusDescription?: string;
    paymentProviderDescription?: string;  
    fullName?: string;
    street?: string;
    streetNumber?: string;
    city?: string
    plz?: string;
    country?: string;
    externalTransactionId?: string;
    paymentProvider?: PaymentProvider;
    amountInCent: number;
    phoneNumber?: string;
    currency: string;
    summaryResponse: ISummaryResponse;  
  }
  
  export enum OrderStatus {
    Placed = 0,
    Processed = 1,
    CanceledByCustomer = 2,
    CanceledByServiceOperator = 3,
    Paid = 4,
    Confirmed = 5,
    FailedToSendConfirmationError = 6,
    Created = 7,
  }
  
  export class OrderInfo {
    constructor(public order: Order) {}
  
    public get statusDescription(): string {
      console.log(
        'stat in is :' + JSON.stringify(this.order) + ' whereas confirmed is ' + OrderStatus.Placed,
      );
      switch (this.order.status) {
        case OrderStatus.CanceledByCustomer:
          return 'widerrufen';
        case OrderStatus.CanceledByServiceOperator:
          return 'abgelehnt';
        case OrderStatus.Confirmed:
          return 'Bestätigt';
        case OrderStatus.Paid:
          return 'Bezahlt';
        case OrderStatus.Created:
          return 'Nicht abgeschlossen';
        case OrderStatus.Placed:
          return 'Bestellung eingegangen';
        case OrderStatus.FailedToSendConfirmationError:
          return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
        default:
          return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
      }
    }
  
    public get paymentProviderDescription(): string {
      switch (this.order.paymentProvider) {
        case PaymentProvider.bill:
          return 'Rechnung';
        case PaymentProvider.paypalCheckout:
          return 'Paypal/Kreditkarte';
        default:
          return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
      }
    }
  }

  export enum PaymentProvider{
    bill = 1,
    paypalCheckout
}