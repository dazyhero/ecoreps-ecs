import { DB_CONNECT_INJECTION, ORDER_MODEL } from '../constants/db';
import mongoose, { Connection } from 'mongoose';
import { default as AutoIncrementFactory }  from 'mongoose-sequence';
import { Module } from '@nestjs/core/injector/module';
import { Logger } from '@nestjs/common';
import { PaymentProvider } from './model/order';

const priceInfoSchema = new mongoose.Schema(
  {
    originalValue: { type: Number, required: false, default: null, sparse: true },
    value: { type: Number, required: true },
    savingInPercent: { type: Number, required: false, sparse: true },
  },
  { _id: false },
);

const courseOffer = new mongoose.Schema(
  {
    id: { type: String, required: true, unique: false },
    title: { type: String, required: true, unique: false },
    product_category: { type: String, required: true, unique: false },
    state: { type: Number, required: true, unique: false },
    price: priceInfoSchema
  },
  { _id: false },
);

const addonOffer = new mongoose.Schema(
  {
    id: { type: String, required: true, unique: false },
    title: { type: String, required: true, unique: false },
    description: { type: String, required: true, unique: false },
    state: { type: Number, required: true, unique: false },
    price: priceInfoSchema,
  },
  { _id: false },
);


export const OrderSchema = new mongoose.Schema(
    {       
      _id: Number, 
      user_id: { type: String, required: true, unique: false },       
      externalTransactionId: {type: String, required: false},
      fullName: { type: String, required: false },
      street: { type: String, required: false },
      streetNumber: { type: String, required: false },
      city: { type: String, required: false },
      plz: { type: String, required: false },
      country: { type: String, required: false },      
      userId: { type: String, required: true },      
      status: { type: Number },
      phoneNumber: { type: String, required: false },
      paymentProvider: {
        type: Number,
        required: true,
        default: PaymentProvider.bill,
      },      
      currency: { type: String, required: false },
      amountInCent: { type: Number, required: true, default: 0 },
      summaryResponse: {
        currency: { type: String, required: false },
        courseOffers: [{}],
        addonOffers: [{}],
        effectiveVouchers: { type: [String], required: false },
        totalVoucherDiscount: { type: Number, required: false },
        total: priceInfoSchema
      } 
    },    
    {
      _id: false,
      timestamps: true,
      toJSON: {
        virtuals: true
      }
    }
  ); 
 
var counterRegistrations = {};

export const orderProvider = [
    {
      provide: ORDER_MODEL,
      useFactory: (connection: Connection) => {
        const log = new Logger();                
        if(!counterRegistrations[connection.name]){
          const AutoIncrement =  AutoIncrementFactory(connection);     
          OrderSchema.plugin(AutoIncrement, {id: 'order', inc_field: '_id'});          
          counterRegistrations[connection.name] = true;
        }         
        return connection.model(ORDER_MODEL, OrderSchema)
      },
      inject: [DB_CONNECT_INJECTION],
    },
  ];


