import { Module } from '@nestjs/common';
import { OrderController } from './controllers/order.controller';
import { OrderService } from './services/order.service';
import { MongooseModule as MongooseCustomModule } from '../mongoose/mongoose.module';
import { orderProvider } from './order.provider'
import { coursesProvider } from '../shared/providers/courses.provider'
import { SharedModule } from '../shared/shared.module';
import { usedVoucherProvider } from './usedVoucher.provider';
import { PaypalClient } from './services/paypal.service';

@Module({
  imports: [MongooseCustomModule, SharedModule],
  controllers: [OrderController],
  providers: [OrderService, ...orderProvider,　...usedVoucherProvider, ...coursesProvider, PaypalClient],
})
export class OrderModule {}
 