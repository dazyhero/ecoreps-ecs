import { Controller, Get, Delete, Param, Post, Body, Headers } from '@nestjs/common';
import { number, string } from 'joi';
import { resolve } from 'url';
import { UserClaimData, UserId, IsUserAdmin } from '../../shared/decorators/user-claim.decorator';
import { IAuth0User } from '../../shared/types/user.types';
import { IVoucherValidationResponse } from '../order.interface';
import { OrderService } from '../services/order.service';
import { ISummaryRequest, ISummaryResponse, OrderRequest, default as IOrder } from '../model/order';
import { ClientId } from '../../../../shared/src';

@Controller('profile/order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Get('order/voucher/validate/:voucherCode')
  public redeemVoucher(
    @Param('voucherCode') voucherCode: string,
  ): Promise<IVoucherValidationResponse> {
    return this.orderService.validateVoucher(voucherCode);
  }

  @Post('summary')
  public summary(
    @UserClaimData() userData: IAuth0User,
    @Body() body: ISummaryRequest,
  ): Promise<ISummaryResponse> {
    return this.orderService.getSummary(body, userData);
  }

  @Post('billing')
  public orderByBilling(
    @UserClaimData() userData: IAuth0User,
    @Body() body: OrderRequest,
    @Headers('clientid') clientid: ClientId,
  ): Promise<IOrder> {
    return this.orderService.orderByBilling(body, userData, clientid);
  }

  @Post('paypal')
  public createPaypalOrder(
    @UserClaimData() userData: IAuth0User,
    @Body() body: OrderRequest,
  ): Promise<IOrder> {
    return this.orderService.createPaypalOrder(body, userData);
  }

  @Get('paypal/capture/:transactionId')
  public capturePaypalOrder(
    @UserClaimData() userData: IAuth0User,
    @Param('transactionId') transactionId: string,
    @Headers('clientid') clientid: ClientId,
  ): Promise<IOrder> {
    return this.orderService.capturePaypalOrder(transactionId, userData, clientid);
  }
}
