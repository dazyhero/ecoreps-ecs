import { Logger } from '@nestjs/common';
import { get as GetConfig } from 'config';
import request = require('request');
import { Injectable } from '@nestjs/common';
import { ICapturePaymentResponse } from '../../../../shared/src';

const checkoutNodeJssdk = require('@paypal/checkout-server-sdk');

require('https').globalAgent.options.ca = require('ssl-root-cas').create();

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '1';

interface IPaymentAuthorizeRequest {
  priceInCent: number;
  currency: string;
}

@Injectable()
export class PaypalClient {
  'use strict';

  private logger = new Logger(PaypalClient.name);
  /**
   *
   * PayPal Node JS SDK dependency
   */

  /**
   *
   * Returns PayPal HTTP client instance with environment that has access
   * credentials context. Use this instance to invoke PayPal APIs, provided the
   * credentials have access.
   */
  public client() {
    return new checkoutNodeJssdk.core.PayPalHttpClient(this.environment());
  }

  public async getToken(): Promise<any> {
    const logger = this.logger;
    return new Promise<any>((resolve, reject) => {
      const url = this.environment().url;
      const clientId = this.environment().clientId;
      const password = this.environment().clientSecret;

      const basicString = Buffer.from(clientId + ':' + password).toString('base64');

      var options = {
        method: 'POST',
        url: url + '/v1/oauth2/token' + '?grant_type=client_credentials',
        headers: {
          Authorization: 'Basic ' + basicString,
        },
      };

      request(options, function(error, response, body) {
        if (error) return reject(new Error(error));
        logger.log('Result token request:' + body);
        logger.log(body);
        if (body.error) {
          reject(body.error);
          return;
        }
        logger.log(JSON.parse(body));
        resolve(JSON.parse(body));
      });
    });
  }

  public async authorizePayment(
    req: IPaymentAuthorizeRequest,
    description: string,
    token: string,
  ): Promise<any> {
    const logger = this.logger;

    return new Promise<any>((resolve, reject) => {
      var request = require('request');

      let currencyCode = { Euro: 'EUR' }[req.currency] || req.currency;

      const body = {
        intent: 'CAPTURE',
        purchase_units: [
          {
            description: description,
            amount: { currency_code: currencyCode, value: (req.priceInCent / 100).toString() },
          },
        ],
      };

      let url = this.environment().url;

      var options = {
        rejectUnauthorized: false,
        method: 'POST',
        url: url + '/v2/checkout/orders',
        headers: {
          'cache-control': 'no-cache',
          Connection: 'keep-alive',
          'Cache-Control': 'no-cache',
          Accept: '*/*',
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
        body: body,
        json: true,
      };

      request(options, function(error, response, body) {
        logger.log('response closure');
        if (error) reject(new Error(error));
        logger.log('Result authorize pm:' + JSON.stringify(body));
        resolve(body);
      });
    });
  }

  public async capturePayment(
    paypalOrderId: string,
    token: string,
  ): Promise<ICapturePaymentResponse> {
    return new Promise<ICapturePaymentResponse>((resolve, reject) => {
      this.logger.log('enter closure');
      var request = require('request');
      let url = this.environment().url;
      var options = {
        rejectUnauthorized: false,
        method: 'POST',
        //url: url + '/v2/checkout/orders/' + paypalOrderId + "/authorize",
        url: url + '/v2/checkout/orders/' + paypalOrderId + '/capture',
        headers: {
          'cache-control': 'no-cache',
          Connection: 'keep-alive',
          'Cache-Control': 'no-cache',
          Accept: '*/*',
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
        body: { final_capture: true },
        json: true,
      };

      const logger = this.logger;

      request(options, function(error, response, body) {
        logger.log('response closure 1: ' + JSON.stringify(error));
        logger.log('response closure 2: ' + JSON.stringify(response));
        logger.log('Result capture pm:' + body);
        if (error) reject(new Error(error));
        logger.log(body);
        resolve(body as ICapturePaymentResponse);
      });
    });
  }

  /**
   *
   * Set up and return PayPal JavaScript SDK environment with PayPal access credentials.
   * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
   *
   */
  public environment() {
    const conf = GetConfig('Service.server.paypal');
    this.logger.log('pp conf' + JSON.stringify(conf));
    return conf;
  }

  public async prettyPrint(jsonData, pre = '') {
    let pretty = '';

    function capitalize(string) {
      return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    for (let key of jsonData) {
      if (jsonData.hasOwnProperty(key)) {
        if (isNaN(key as number)) pretty += pre + capitalize(key) + ': ';
        else pretty += pre + (parseInt(key) + 1) + ': ';
        if (typeof jsonData[key] === 'object') {
          pretty += '\n';
          pretty += await this.prettyPrint(jsonData[key], pre + '    ');
        } else {
          pretty += jsonData[key] + '\n';
        }
      }
    }
    return pretty;
  }
}
