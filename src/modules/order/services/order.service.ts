import _ from 'lodash';
import { Document, Model, Types } from 'mongoose';
import {
  Injectable,
  NotFoundException,
  Inject,
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { IAuth0User, ICourse } from '../../shared/types/user.types';
import { COURSE_MODEL, ORDER_MODEL, USEDVOUCHER_MODEL } from '../../constants/db';
import { FilterQuery } from 'mongodb';
import * as jwt from 'jsonwebtoken';
import { DirectusClient } from '../../shared/services/directus.client';
import { fakeServer } from 'sinon';
import {
  IUsedVoucher,
  VoucherError,
  IVoucher,
  IVoucherValidationResponse,
  VoucherErrorType,
} from '../order.interface';
import { default as IOrder, OrderRequest, OrderStatus, PaymentProvider } from '../model/order';
import { min } from 'moment';
import { Logger } from '@nestjs/common';
import { Mailer } from '../../shared/services/mailer.service';
import { use } from 'chai';
import { badImplementation, notFound, notImplemented } from 'boom';
import { PaypalClient } from './paypal.service';
import { Auth0ManagementService } from '../../shared/services/auth0.client';
import {
  ISummaryResponse,
  ICourse as SharedCourse,
  ISummaryRequest,
  CourseOfferState,
  IAddonOffer,
  ICourseItem,
  IHasOfferState,
  IHasPriceInfo,
  ICapturePaymentResponse,
  ClientId,
  ClientTypeId,
} from '../../../../shared/src';

interface Address {
  fullName: string;
  street: string;
  streetNumber: string;
  city: string;
  plz: string;
  country: string;
}

@Injectable()
export class OrderService {
  constructor(
    @Inject(COURSE_MODEL) private courseModel: Model<ICourse>,
    @Inject(ORDER_MODEL) private orderModel: Model<IOrder & Document>,
    @Inject(USEDVOUCHER_MODEL) private usedVoucherModel: Model<IUsedVoucher>,
    private paypalClient: PaypalClient,
    private directusClient: DirectusClient,
    private mailService: Mailer,
  ) {}

  private async voucherIsUsed(voucher: IVoucher): Promise<Boolean> {
    const usedVoucher = await this.usedVoucherModel.findOne({ id: voucher.id });
    return !!usedVoucher;
  }

  private async redeem(voucher: IVoucher): Promise<IUsedVoucher> {
    const usedVoucher = await this.usedVoucherModel.create<IUsedVoucher>({
      ...voucher,
      date_redeemed: new Date(),
    });
    return usedVoucher;
  }

  private readonly logger = new Logger(OrderService.name);

  private async redeemIfNeccesary(code: string): Promise<void> {
    const directusVouchers = await this.directusClient.getItems<IVoucher>('vouchers', '*', {
      code: code,
    });
    if (!directusVouchers || directusVouchers.length == 0 || !directusVouchers[0].single_use) {
      return;
    }
    await this.redeem(directusVouchers[0]);
  }

  private async checkIfVoucherIsValidAndUnused(
    voucherCode: string,
  ): Promise<{
    valid: boolean;
    discountInCents?: number;
  }> {
    // 1. check if voucher code is provided
    if (!voucherCode) {
      throw new VoucherError(VoucherErrorType.codeDoesNotExist);
    }

    // 2. check if voucher code exists in directus
    const directusVouchers = await this.directusClient.getItems<IVoucher>('vouchers', '*', {
      code: voucherCode,
    });

    if (!directusVouchers || directusVouchers.length == 0) {
      throw new VoucherError(VoucherErrorType.codeDoesNotExist);
    }

    // 3. check if voucher was used

    const voucher = directusVouchers[0];

    if (voucher.single_use) {
      if (await this.voucherIsUsed(voucher)) {
        throw new VoucherError(VoucherErrorType.voucherIsUsed);
      }
    }

    return {
      valid: true,
      discountInCents: parseInt(voucher.discount_in_cents),
    };
  }

  private static voucherResponseFromError(error: Error): IVoucherValidationResponse {
    if (error instanceof VoucherError) {
      return { valid: false, message: error.message };
    } else {
      return { valid: false, message: 'Unbekannter Fehler' };
    }
  }

  public async validateVoucher(voucherCode): Promise<IVoucherValidationResponse> {
    try {
      return await this.checkIfVoucherIsValidAndUnused(voucherCode);
    } catch (error) {
      return OrderService.voucherResponseFromError(error);
    }
  }

  public async getAllCoursesInCategory(courseId: string): Promise<SharedCourse[]> {
    const course = await this.directusClient.getItem<SharedCourse>('course', courseId);
    const category = (course as any).product_category;
    const coursesInCategory = await this.directusClient.getItems<SharedCourse>(
      'course',
      'url_slug',
      { product_category: category },
    );
    const slugs: string[] = coursesInCategory.map(course => course.url_slug);
    const coursesFromDb: SharedCourse[] = (
      await this.courseModel.find({ url_slug: { $in: slugs } })
    ).map(course => course.toObject());
    return coursesFromDb;
  }

  public async getCourseCategoryDetails(
    courseId: string,
    selectedCourseIds: string[],
    user: IAuth0User,
  ): Promise<(SharedCourse & IHasOfferState)[]> {
    const courses = await this.getAllCoursesInCategory(courseId);
    const purchasedCourses = user['app_metadata'].purchasedCourses;

    return courses.map(course => {
      const slug = course.url_slug;
      const id = course.id;

      this.logger.log('mapping course: ' + course.title + ' of category of: ' + courseId);

      //      const inCart = (selectedCourseIds || []).indexOf(id) >= 0 || (selectedCourseIds || []).indexOf(slug) >= 0 || slug == courseId || id == courseId;
      const inCart =
        (selectedCourseIds || []).indexOf(id) >= 0 || (selectedCourseIds || []).indexOf(slug) >= 0;

      const purchased = purchasedCourses.indexOf(course._id.toString()) >= 0;

      let orderState: CourseOfferState;
      if (purchased) {
        orderState = CourseOfferState.purchased;
      } else if (inCart) {
        orderState = CourseOfferState.inCart;
      } else {
        orderState = CourseOfferState.offered;
      }

      return {
        ...course,
        state: orderState,
      };
    });
  }

  public async getAddonsForCategory(
    categoryId: string,
    selectedAddonIds: string[],
  ): Promise<IAddonOffer[]> {
    let category = await this.directusClient.getItem<any>(
      'product_category',
      parseInt(categoryId),
      '*,addons.*,addons.addon_id.*.*,addons.addon_id.price.*',
    );
    return category.addons.map(addon => {
      let originalValue = addon.addon_id.price.price;
      let value = originalValue - addon.addon_id.price.discount_everyone;
      let savingInPercent: number = 0;
      if (originalValue > 0) {
        savingInPercent = Math.round(((originalValue - value) / originalValue) * 100);
      }
      return {
        id: addon.addon_id.id,
        title: addon.addon_id.title,
        description: addon.addon_id.infotext,
        price: {
          value: addon.addon_id.price.price,
          originalValue: originalValue,
          savingInPercent: savingInPercent,
        },
        state:
          selectedAddonIds && !!selectedAddonIds.find(addonId => addonId == addon.addon_id.id)
            ? CourseOfferState.inCart
            : CourseOfferState.offered,
      };
    });
  }
  public async applyDiscountsToCourses(
    courses: (SharedCourse & IHasOfferState)[],
    categoryId: any,
  ): Promise<(ICourseItem & IHasOfferState & IHasPriceInfo)[]> {
    let category = await this.directusClient.getItem<any>('product_category', parseInt(categoryId));

    let discount = category.bundle_discount;
    let numberOfPurchasedCourses = courses.filter(
      course => course.state == CourseOfferState.purchased,
    ).length;
    let numberOfSelectedCourses = courses.filter(course => course.state == CourseOfferState.inCart)
      .length;

    const applyDiscountForAllCourses = numberOfPurchasedCourses > 0;

    let applyDiscountForSecondSelected = numberOfSelectedCourses > 0;

    /**
       * disable sorting by offer state
       * .sort((a, b) => {
      if (a.state != CourseOfferState.offered && b.state == CourseOfferState.offered ) {
        return -1;
      } else {
        return 0;
      }
    })
       */

    var skipCount = 0;
    return courses.map((course: SharedCourse & IHasOfferState) => {
      let originalValue = course.price.price;
      if (course.state == CourseOfferState.inCart && applyDiscountForSecondSelected) {
        skipCount++;
      }
      const appliedDiscount =
        applyDiscountForAllCourses ||
        (applyDiscountForSecondSelected &&
          (course.state != CourseOfferState.inCart || skipCount != 1))
          ? discount
          : 0;
      if (skipCount == 1 && appliedDiscount) {
        skipCount++;
      }
      let value = originalValue - course.price.discount_everyone - appliedDiscount;
      let savingInPercent: number = 0;
      if (originalValue > 0) {
        savingInPercent = Math.round(((originalValue - value) / originalValue) * 100);
      }

      return {
        title: course.title,
        id: course.url_slug,
        product_category: ((course as unknown) as any).product_category.id,
        state: course.state,
        contactDescription: OrderService.contactDescription(
          course.whatsapp_service,
          course.contact_mail,
        ),
        price: {
          savingInPercent,
          originalValue,
          value,
        },
      } as ICourseItem & IHasOfferState & IHasPriceInfo;
    });
  }

  private static contactDescription(phone: string, mail: string): string {
    let description = '';
    if (!!mail) {
      description += 'Mail-Service: ' + mail;
    }
    if (!!phone) {
      description += (!!phone ? ', Whatsapp-Service: ' : 'Whatsapp-Service: ') + phone;
    }
    return description;
  }

  private static currencyCode(currencyString: string): string {
    return { Euro: 'EUR' }[currencyString] || currencyString || 'EUR';
  }

  public async getSummary(
    summaryRequest: ISummaryRequest,
    userData: IAuth0User,
  ): Promise<ISummaryResponse> {
    // 1. course offers
    let offersForCourse = await this.getCourseCategoryDetails(
      summaryRequest.course,
      summaryRequest.selectedCourseIds,
      userData,
    );

    if (offersForCourse.length <= 0) {
      throw new NotFoundException('No course in offer');
    }

    let currency = OrderService.currencyCode(offersForCourse[0].price.currency);

    let itemsWithPrices = await this.applyDiscountsToCourses(
      offersForCourse,
      (offersForCourse[0] as any).product_category.id,
    );

    // 2. Get Addons
    const addons = await this.getAddonsForCategory(
      (offersForCourse[0] as any).product_category.id,
      summaryRequest.selectedAddonIds,
    );

    //  3. Calculate total along with vouchers and addons
    const voucherCode = summaryRequest.activeVoucher;
    var effectiveVoucherCodes: string[] = [];

    let voucherDiscount: number = 0;

    if (voucherCode && voucherCode.length > 0) {
      try {
        const voucher = await this.validateVoucher(voucherCode);
        if (voucher.valid) {
          effectiveVoucherCodes.push(voucherCode);
          voucherDiscount += voucher.discountInCents || 0;
        }
      } catch {
        this.logger.error('Tried to apply invalid voucher code: ' + voucherCode);
      }
    }

    let totalPrice = 0;
    let totalOriginalPrice = 0;

    for (let item of itemsWithPrices) {
      if (!(item.state == CourseOfferState.inCart)) {
        continue;
      }
      totalPrice += item.price.value;
      totalOriginalPrice += item.price.originalValue;
    }

    for (let addon of addons) {
      if (!(addon.state == CourseOfferState.inCart)) {
        continue;
      }
      totalPrice += addon.price.value;
      totalOriginalPrice += addon.price.originalValue;
    }
    totalPrice = totalPrice - voucherDiscount;

    let totalSavingInPercent = 0;

    totalPrice = Math.max(0, totalPrice);

    if (totalOriginalPrice > 0) {
      totalSavingInPercent = Math.round(
        ((totalOriginalPrice - totalPrice) / totalOriginalPrice) * 100,
      );
    }

    return {
      currency,
      courseOffers: itemsWithPrices,
      addonOffers: addons,
      effectiveVouchers: effectiveVoucherCodes,
      totalVoucherDiscount: voucherDiscount,
      total: {
        originalValue: totalOriginalPrice,
        value: totalPrice,
      },
    } as ISummaryResponse;
  }

  public async capturePaypalOrder(
    transactionId: string,
    userData: IAuth0User,
    type: ClientId,
  ): Promise<IOrder> {
    let order = await this.orderModel.findOne({
      externalTransactionId: transactionId,
      paymentProvider: PaymentProvider.paypalCheckout,
      status: OrderStatus.Created,
    });

    if (!order) {
      throw notFound('no order present for payment.');
    }

    const paypal = this.paypalClient;

    const token = (await paypal.getToken()).access_token;

    const captured: ICapturePaymentResponse = await paypal.capturePayment(transactionId, token);

    order.status = OrderStatus.Paid;

    const payer = captured.payer;

    const shipping = captured.purchase_units[0].shipping;

    order.fullName = order.fullName || payer.name.given_name + ' ' + payer.name.surname;
    order.country = payer.address.country_code;
    order.street = shipping.address.address_line_1;
    order.city = shipping.address.admin_area_2;
    order.plz = shipping.address.postal_code;

    await order.save();

    const auth0 = await Auth0ManagementService.instance(ClientTypeId[type]);

    const slugs = order.summaryResponse.courseOffers
      .filter(offer => offer.state == CourseOfferState.inCart)
      .map(offer => offer.id);

    const courseIDsToGrant: string[] = (
      await this.courseModel.find({ url_slug: { $in: slugs } })
    ).map(course => course.toObject()._id.toString());

    await auth0.grantAccess(userData, courseIDsToGrant);

    await this.mailService.notifyPaypalOrderPaid(order, userData, type);

    return order;
  }

  public async createPaypalOrder(
    orderRequest: OrderRequest,
    userData: IAuth0User,
  ): Promise<IOrder> {
    const summary: ISummaryResponse = await this.getSummary(orderRequest.summary, userData);

    if (summary.total.value != orderRequest.expectedTotal) {
      throw new BadRequestException('Total doesn´t match, please retrieve a new summary');
    }

    let orderData = OrderService.orderFromSummaryAndAddress(
      summary,
      orderRequest.billing as Address,
      orderRequest.phoneNumber,
      userData.user_id,
      orderRequest.summary.course,
      PaymentProvider.paypalCheckout,
    );

    const paypal = this.paypalClient;

    try {
      let token = (await paypal.getToken()).access_token;

      if (!token) {
        throw new InternalServerErrorException('token fetch failed');
      }

      let description =
        'Online-Kurse: ' + summary.courseOffers.map(offer => offer.title).join(', ');

      let pptransaction = await paypal.authorizePayment(
        { priceInCent: summary.total.value, currency: summary.currency || 'EUR' },
        description,
        token,
      );

      this.logger.log('got pp transaction:' + JSON.stringify(pptransaction));

      orderData.externalTransactionId = pptransaction.id;

      const order = new this.orderModel(orderData);

      await order.save();

      return order.toObject();
    } catch (e) {
      throw badImplementation('error creating order: ' + e.toString());
    }
  }

  static orderFromSummaryAndAddress(
    summary: ISummaryResponse,
    address: Address,
    phoneNumber: string,
    userId: string,
    originalCourse: string,
    paymentProvider: PaymentProvider,
  ): IOrder {
    return ({
      createdAt: new Date().getTime(),
      course: originalCourse,
      summaryResponse: summary,
      currency: summary.currency,
      phoneNumber: phoneNumber || 'keine Angabe',
      userId: userId,
      status:
        paymentProvider == PaymentProvider.paypalCheckout
          ? OrderStatus.Created
          : OrderStatus.Placed,
      paymentProviderDescription: 'invoice',
      fullName: address.fullName,
      street: address.street,
      streetNumber: address.streetNumber,
      city: address.city,
      plz: address.plz,
      country: address.country,
      paymentProvider: paymentProvider,
      amountInCent: summary.total.value,
      user_id: userId,
    } as unknown) as IOrder;
  }

  public async orderByBilling(
    orderRequest: OrderRequest,
    userData: IAuth0User,
    type: ClientId,
  ): Promise<IOrder> {
    const summary: ISummaryResponse = await this.getSummary(orderRequest.summary, userData);

    if (summary.total.value != orderRequest.expectedTotal) {
      throw new BadRequestException('Total doesn´t match, please retrieve a new summary');
    }

    const orderData = OrderService.orderFromSummaryAndAddress(
      summary,
      orderRequest.billing as Address,
      orderRequest.phoneNumber,
      userData.user_id,
      orderRequest.summary.course,
      PaymentProvider.bill,
    );

    const order = new this.orderModel(orderData);

    await order.save();

    const slugs = summary.courseOffers
      .filter(offer => offer.state == CourseOfferState.inCart)
      .map(offer => offer.id);

    const courseIDsToGrant: string[] = (
      await this.courseModel.find({ url_slug: { $in: slugs } })
    ).map(course => course.toObject()._id.toString());

    const auth0 = await Auth0ManagementService.instance(ClientTypeId[type]);

    await auth0.grantAccess(userData, courseIDsToGrant);

    for (let code of summary.effectiveVouchers || []) {
      await this.redeemIfNeccesary(code);
    }

    await this.mailService.notifyBillingOrderReceived(order, userData, type);

    return order;
  }
}
