import { ICourse as SharedCourse, IChapter as SharedChapter } from '../../../../shared/src';
import { Document, Schema } from 'mongoose';
import { Collection } from 'mongodb';
import SharedProfile from '../../../../src/model/profile';

export type UserData = {
  userName: string;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  mobilePhoneNumber: string;
};

export type Auth0AppMetaData = {
  roles?: string[];
  purchasedCourses?: string[];
};

export type IAuth0User = Document &
  Collection &
  SharedProfile & {
    purchasedCourses?: string[];
    user_id?: string;
    lastLogin?: string;
    app_metadata?: Auth0AppMetaData;
    email?: string;
    audience?: string;
    name?: string;
    nickname?: string;
    value?: any;
    anonymousId?: string;
    mainUniversity: {
      id: any;
      url_slug: string;
      name: string;
    };
  };

export type ICourse = Collection &
  SharedCourse &
  Document & {
    value: any;
  };

export type IChapter = SharedChapter &
  Document & {
    course?: Schema.Types.ObjectId | ICourse;
    path?: string;
  };
