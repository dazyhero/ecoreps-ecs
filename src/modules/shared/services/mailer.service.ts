import { Injectable, Logger } from '@nestjs/common';
import { MailerModule, MailerService } from '@nestjs-modules/mailer';
import { get as GetConfig } from 'config';
import Order from '../../order/model/order';
import { IAuth0User } from '../types/user.types';
import { billingConfirmationTemplate } from '../../order/mails/billing.confirmation.template';
import { text } from 'express';
import { paypalConfirmationTemplate } from '../../order/mails/paypal.confirmation.template';
import { ClientId, ClientTypeId } from '../../../../shared/src';

@Injectable()
export class Mailer {
  constructor(private mailer: MailerService) {}

  private async sendMail(
    to: string[],
    subject: string,
    text: string,
    html: string,
    clientString: string,
  ): Promise<void> {
    /*
const mailServer = GetConfig('Service.server.mailSMTP.server');
const mailPort = GetConfig('Service.server.mailSMTP.port');
const mailUserName = GetConfig('Service.server.mailSMTP.username');
const mailPassword = GetConfig('Service.server.mailSMTP.password');


    const mailerModule = MailerModule.forRoot({transport: {
      host: mailServer,
      port: mailPort,
      secure: false,
      auth: {
        user: mailUserName,
        pass: mailPassword,
      },             
    },
    defaults: {
      from: 'Ecoreps E-Learning <noreply@ecoreps.de>',
    }});*/

    const logger = new Logger('mailer');

    const from = GetConfig('Service.server.mailFrom' + clientString);

    logger.log('sends order mail from: ' + from);

    await this.mailer.sendMail({
      from: from,
      to: to.shift(),
      bcc: to.join(','),
      subject,
      text,
      html,
    });
  }

  public notifyBlockedLogins(to: string, clientId: ClientId): void {
    const securityAddress = GetConfig('Service.server.securityAddress' + ClientTypeId[clientId]);
    const recipients = [to, securityAddress];
    const clientString = ClientTypeId[clientId];
    const subject = clientString + ': Sicherheitsproblem mit deinem Account';
    const text =
      'Lieber ' +
      clientString +
      '-user, <br/> ' +
      'leider mussten wir auf deinem Account mit der Email-Adresse ' +
      to +
      ' ungewöhnliche Aktivitäten feststellen (z.B. Logins von verschiedenen Nutzern) <br/>' +
      'Wir werden deinen Account untersuchen und gegebenenfalls mit dir Kontakt aufnehmen. <br/>' +
      'Viele Grüße <br/>' +
      +clientString +
      ' Team';

    this.sendMail(recipients, subject, null, text, clientString);
  }

  public async notifyBillingOrderReceived(order: Order, user: IAuth0User, clientId: ClientId) {
    const clientString = ClientTypeId[clientId];
    const orderAddress = GetConfig('Service.server.orderAddress' + clientString);
    const userMail = user.email;
    const body = billingConfirmationTemplate(order);
    const textBody = body.replace(/<\/?[^>]+(>|$)/g, '');
    const htmlBody = body.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br/>' + '$2');
    await this.sendMail(
      [userMail, orderAddress],
      'Bestellbestätigung: #' + order._id,
      textBody,
      htmlBody,
      clientString,
    );
  }

  public async notifyPaypalOrderPaid(order: Order, user: IAuth0User, clientId: ClientId) {
    const clientString = ClientTypeId[clientId];
    const orderAddress = GetConfig('Service.server.orderAddress' + clientString);
    const userMail = user.email;
    const body = paypalConfirmationTemplate(order);
    const textBody = body.replace(/<\/?[^>]+(>|$)/g, '');
    const htmlBody = body.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br/>' + '$2');
    await this.sendMail(
      [userMail, orderAddress],
      'Bestellbestätigung #' + order._id,
      textBody,
      htmlBody,
      clientString,
    );
  }
}
