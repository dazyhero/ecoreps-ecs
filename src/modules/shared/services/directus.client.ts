import request = require('request');
import Boom = require('boom');

import { get as GetConfig } from 'config';
import { Injectable, Scope, Inject } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { PublishState } from '../../../../shared/src';

const clientId = {
  100: 'Ecoreps',
  200: 'StudyPrime',
};
@Injectable({ scope: Scope.REQUEST })
export class DirectusClient {
  private type: string;
  constructor(@Inject(REQUEST) req: Request) {
    const { clientid } = req.headers;
    this.type = clientId[+clientid];
  }

  private readonly logger = new Logger(DirectusClient.name);

  private get baseURL(): string {
    const directusScheme = GetConfig(`Service.server.directus${this.type}.scheme`);
    const directusHost = GetConfig(`Service.server.directus${this.type}.host`);
    const directusPort = GetConfig(`Service.server.directus${this.type}.port`);
    const directusProject = GetConfig(`Service.server.directus${this.type}.project`);
    let url =
      directusScheme +
      '://' +
      directusHost +
      (directusPort ? ':' + directusPort : '') +
      '/' +
      directusProject +
      '/';
    return url;
  }

  private get token(): string {
    const directusToken = GetConfig(`Service.server.directus${this.type}.token`);
    return directusToken || '';
  }

  public getItems = async <T>(
    itemName: string,
    includeFields: string = null,
    filter: any = {},
  ): Promise<T[]> => {
    let allFilters = { 'filter[status]': PublishState.published };

    for (let key in filter || {}) {
      if (filter.hasOwnProperty(key)) {
        let value = filter[key];
        let k = 'filter[' + key + ']';
        allFilters[k] = value;
      }
    }

    let url = this.baseURL + 'items/' + itemName;
    const token = this.token;

    const logger = this.logger;

    let promise = new Promise<T[]>((resolve, reject) => {
      var options = {
        method: 'GET',
        url: url,
        qs: { fields: includeFields, ...allFilters },
        headers: { authorization: 'Bearer ' + this.token },
      };

      this.logger.log('get items requests: ' + JSON.stringify(options));

      request(options, function(error, response, body) {
        if (error) {
          if (error.statusCode == 404) {
            reject(Boom.notFound(itemName));
          } else if (error.statusCode == 403) {
            reject(Boom.forbidden('by directus:' + itemName));
          } else {
            reject(Boom.badImplementation(error));
          }
        }
        let parsedData = JSON.parse(body).data;

        resolve(parsedData as T[]);
      });
    });

    return promise;
  };

  public async getItem<T>(
    itemName: string,
    itemId: number | string,
    includeFields: string = null,
    filter: any = {},
  ): Promise<T> {
    var allFilters = { 'filter[status]': PublishState.published };

    for (let key in filter || {}) {
      if (filter.hasOwnProperty(key)) {
        let value = filter[key];
        let k = 'filter[' + key + ']';
        allFilters[k] = value;
      }
    }
    return new Promise<T>((resolve, reject) => {
      var url = this.baseURL + 'items/' + itemName;

      if (typeof itemId === 'string') {
        url += '?filter[url_slug]=' + (itemId as string);
      } else {
        url += '/' + itemId;
      }

      var options = {
        method: 'GET',
        url: url,
        qs: { fields: includeFields, ...allFilters },
        headers: { authorization: 'Bearer ' + this.token },
      };
      request(options, function(error, response, body) {
        if (response.statusCode == 404) {
          reject(Boom.notFound(itemName));
        }

        if (error) throw Boom.badImplementation(error);
        var parsed = JSON.parse(body).data;
        parsed = Array.isArray(parsed) ? parsed[0] : parsed;
        resolve(parsed as T);
      });
    });
  }
}

export default DirectusClient;
