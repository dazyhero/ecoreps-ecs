import { string } from 'joi';
import { IUserAccessState, ClientType } from '../../../../shared/src';

import { get as GetConfigs } from 'config';
import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { IAuth0User } from '../types/user.types';

var request = require('request');

export interface Auth0AppMetaData {
  roles?: string[];
  purchasedCourses?: string[];
}

@Injectable()
export class Auth0ManagementService {
  public static async instance(type: ClientType): Promise<Auth0ManagementService> {
    let token = await Auth0ManagementService.getBearer(type);
    return new Auth0ManagementService(token, type);
  }

  private _token: string;
  private type: ClientType;

  private constructor(token: string, type: ClientType) {
    this._token = token;
    this.type = type;
  }

  private static async getBearer(type: ClientType) {
    const auth0TenantUrl = GetConfigs(`Service.server.auth0${type}TenantUrl`) + '/oauth/token';
    const auth0ManagementBearerToken = GetConfigs(
      `Service.server.auth0${type}ManagementBearerToken`,
    );
    let promise = new Promise<any>((resolve, reject) => {
      var options = {
        method: 'POST',
        url: auth0TenantUrl,
        headers: { 'content-type': 'application/json' },
        body: auth0ManagementBearerToken,
      };

      const logger = new Logger(Auth0ManagementService.name);

      request(
        options,

        function(error, response, body) {
          if (error) throw new Error(error);
          let object = JSON.parse(body);
          logger.log(object);
          logger.log(object.access_token);
          resolve(object.access_token);
        },
      );
    });
    return promise;
  }

  public async getUserListAndSubscriptionStatesForCourse(
    courseId: string,
    profiles: IAuth0User[],
  ): Promise<IUserAccessState[]> {
    var userIds = profiles.map(p => (p as any).user_id);

    var promises: Promise<IAuth0User[]>[] = [];

    var i,
      j,
      ids,
      chunk = 50;

    for (i = 0, j = userIds.length; i < j; i += chunk) {
      ids = userIds.slice(i, i + chunk);
      promises.push(this.getUserList(ids));
    }
    const logger = new Logger(Auth0ManagementService.name);

    //this works only on NodeJS 11.0 and above
    let list = [].concat.apply(
      [],
      await Promise.all(
        promises.map(p =>
          p.catch(error => logger.error('Error fetching users: ' + JSON.stringify(error))),
        ),
      ),
    );

    let accessList = list.map(user => {
      return {
        access:
          ((user.app_metadata || { purchasedCourses: [] }).purchasedCourses || []).indexOf(
            courseId.toString(),
          ) >= 0,
        email: user.email || user.user_id,
        userId: user.user_id,
      };
    });
    return accessList;
  }

  public async getUserList(ids: string[]): Promise<IAuth0User[]> {
    const logger = new Logger(Auth0ManagementService.name);

    const auth0TenantUrl =
      GetConfigs(`Service.server.auth0${this.type}TenantUrl`) + '/api/v2/users';
    let promise = new Promise<any>((resolve, reject) => {
      var options = {
        method: 'GET',
        url: auth0TenantUrl,
        qs: { q: 'user_id:"' + ids.join('" or "') + '"', search_engine: 'v3', per_page: 100 },
        headers: { authorization: 'Bearer ' + this._token },
      };

      logger.log('searchlist: ' + JSON.stringify("user_id:'" + ids.join("' or '") + "'"));
      request(options, function(error, response, body) {
        logger.log('result of searchlist is: ' + body);
        if (error) throw new Error(error);
        resolve(JSON.parse(body) as IAuth0User[]);
        logger.log(body);
      });
    });

    return promise;
  }

  public async getUserById(userId: string): Promise<IAuth0User> {
    const logger = new Logger(Auth0ManagementService.name);

    const token = this._token;

    const auth0TenantUrl =
      GetConfigs(`Service.server.auth0${this.type}TenantUrl`) + '/api/v2/users/';
    let promise = new Promise<any>((resolve, reject) => {
      var options = {
        method: 'GET',
        url: auth0TenantUrl + userId,
        headers: { authorization: 'Bearer ' + token },
      };

      request(options, function(error, response, body) {
        if (error) throw new Error(error);
        resolve(JSON.parse(body) as IAuth0User);
        logger.log(body);
      });
    });

    return promise;
  }

  public async revokeAccess(user: IAuth0User, courseId: string): Promise<IAuth0User[]> {
    let oldMetadata: Auth0AppMetaData = user.app_metadata || { purchasedCourses: [] };

    let newMetadata = {
      purchasedCourses: [
        ...new Set((oldMetadata.purchasedCourses || []).concat([courseId.toString()])),
      ].filter(cid => cid != courseId),
    };
    let body = {
      app_metadata: newMetadata,
    };

    const logger = new Logger(Auth0ManagementService.name);

    logger.log(body);

    let promise = new Promise<any>((resolve, reject) => {
      const auth0TenantUrl =
        GetConfigs(`Service.server.auth0${this.type}TenantUrl`) + '/api/v2/users/';
      var options = {
        method: 'PATCH',
        url: auth0TenantUrl + user.user_id,
        headers: {
          authorization: 'Bearer ' + this._token,
          'content-type': 'application/json',
        },
        body: body,
        json: true,
      };

      request(options, function(error, response, body) {
        if (error) throw new Error(error);
        resolve(body);
        logger.log(body);
      });
    });

    return promise;
  }

  public async grantAccess(user: IAuth0User, courseIds: string[]): Promise<IAuth0User> {
    const logger = new Logger(Auth0ManagementService.name);

    const token = this._token;

    let oldMetadata: Auth0AppMetaData = user.app_metadata || { purchasedCourses: [] };

    let newMetadata = {
      purchasedCourses: [...new Set((oldMetadata.purchasedCourses || []).concat(courseIds))],
    };
    let body = {
      app_metadata: newMetadata,
    };

    logger.log(body);

    let promise = new Promise<any>((resolve, reject) => {
      const auth0TenantUrl =
        GetConfigs(`Service.server.auth0${this.type}TenantUrl`) + '/api/v2/users/';
      var options = {
        method: 'PATCH',
        url: auth0TenantUrl + user.user_id,
        headers: {
          authorization: 'Bearer ' + token,
          'content-type': 'application/json',
        },
        body: body,
        json: true,
      };

      request(options, function(error, response, body) {
        if (error) throw new Error(error);
        resolve(body);
        logger.log(body);
      });
    });

    return promise;
  }
}
