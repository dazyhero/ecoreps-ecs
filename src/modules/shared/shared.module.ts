import { Module } from '@nestjs/common';
import { Mailer } from './services/mailer.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { DirectusClient } from './services/directus.client';
import { get as GetConfig } from 'config';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import * as path from 'path';

const mailServer = GetConfig('Service.server.mailSMTP.server');
const mailPort = GetConfig('Service.server.mailSMTP.port');
const mailUserName = GetConfig('Service.server.mailSMTP.username');
const mailPassword = GetConfig('Service.server.mailSMTP.password');

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: mailServer,
        port: mailPort,
        secure: false,
        auth: {
          user: mailUserName,
          pass: mailPassword,
        },
        template: {
          dir: path.join(process.env.PWD, 'templates/pages'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
        options: {
          partials: {
            dir: path.join(process.env.PWD, 'templates/partials'),
            options: {
              strict: true,
            },
          },
        },
      },
      defaults: {
        from: 'Ecoreps E-Learning <noreply@ecoreps.de>',
      },
    }),
  ],
  providers: [Mailer, DirectusClient],
  exports: [Mailer, DirectusClient],
})
export class SharedModule {}
