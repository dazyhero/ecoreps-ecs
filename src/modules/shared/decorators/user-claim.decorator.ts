import { createParamDecorator, ExecutionContext, PipeTransform, Type } from '@nestjs/common';

const admin = 'admin';

const extractUserProfileData = (data: string, ctx: ExecutionContext) => {
  const req = ctx.switchToHttp().getRequest();
  if (!req.user) {
    return undefined;
  }
  return req.user[data];
};

export const UserClaimData: (
  ...dataOrPipes: (Type<PipeTransform> | PipeTransform)[]
) => ParameterDecorator = createParamDecorator((data: string, ctx: ExecutionContext): any => {
  return {
    _decodedToken: {
      'https://ecoreps.de/roles': extractUserProfileData('https://ecoreps.de/roles', ctx),
      'https://ecoreps.de/purchased_courses': extractUserProfileData(
        'https://ecoreps.de/purchased_courses',
        ctx,
      ),
      'https://example.com/geoip': extractUserProfileData('https://example.com/geoip', ctx),
      given_name: extractUserProfileData('given_name', ctx),
      family_name: extractUserProfileData('family_name', ctx),
      nickname: extractUserProfileData('nickname', ctx),
      name: extractUserProfileData('name', ctx),
      picture: extractUserProfileData('picture', ctx),
      locale: extractUserProfileData('locale', ctx),
      updated_at: extractUserProfileData('updated_at', ctx),
      email: extractUserProfileData('email', ctx),
      email_verified: extractUserProfileData('email_verified', ctx),
      iss: extractUserProfileData('iss', ctx),
      sub: extractUserProfileData('sub', ctx),
      aud: extractUserProfileData('aud', ctx),
      iat: extractUserProfileData('iat', ctx),
      exp: extractUserProfileData('exp', ctx),
      at_hash: extractUserProfileData('at_hash', ctx),
      nonce: extractUserProfileData('nonce', ctx),
    },
    app_metadata: {
      purchasedCourses: extractUserProfileData('https://ecoreps.de/purchased_courses', ctx) || [],
      roles: extractUserProfileData('https://ecoreps.de/roles', ctx) || [],
    },
    email: extractUserProfileData('email', ctx),
    user_id: extractUserProfileData('user_id', ctx) || extractUserProfileData('sub', ctx),
    last_login: extractUserProfileData('iat', ctx),
    audience: extractUserProfileData('audience', ctx),
    name: extractUserProfileData('name', ctx),
    nickname: extractUserProfileData('nickname', ctx),
  };
});

export const UserId: (
  ...dataOrPipes: (Type<PipeTransform> | PipeTransform)[]
) => ParameterDecorator = createParamDecorator(
  (data: string, ctx: ExecutionContext): any =>
    extractUserProfileData('user_id', ctx) || extractUserProfileData('sub', ctx),
);

export const IsUserAdmin: (
  ...dataOrPipes: (Type<PipeTransform> | PipeTransform)[]
) => ParameterDecorator = createParamDecorator((data: string, ctx: ExecutionContext): any => {
  const roles = extractUserProfileData('https://ecoreps.de/roles', ctx);
  return roles && roles.includes(admin);
});
