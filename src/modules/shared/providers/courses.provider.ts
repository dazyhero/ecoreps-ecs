import mongoose, { Connection } from 'mongoose';
import { DB_CONNECT_INJECTION, COURSE_MODEL } from '../../constants/db';
import { ChapterSchema } from './chapter.provider';

export const ColumnSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    chapter_style: { type: String, required: true },
    id: { type: String, required: true, unique: false },
    order: { type: Number, required: true, unique: false, default: 0 },
    progress_tracking: { type: Number, required: true },
    show_chapter_title: { type: Boolean, required: true },
    enforce_hierarchical_chapter_structure: { type: Boolean, required: true },
    _chapters: {
      type: [ChapterSchema],
      default: [],
      required: true,
    },
    show_chapter_indices: { type: Boolean, required: true },
    stats: {
      chapterCount: { type: Number, default: 0 },
      sectionCount: { type: Number, default: 0 },
      progressAverageUnderstood: { type: Number, default: 0 },
      progressAverageProcessed: { type: Number, default: 0 },
    },
  },
  { strict: false },
);

export const CourseSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    image: String,
    contact_mail: { type: String, required: false },
    contact_phone: { type: String, required: false },
    externalId: { type: String, required: false, default: null, unique: true },
    hasPreview: { type: Boolean, required: false, default: 0 },
    coursePreviewType: {
      type: Number,
      required: false,
      default: 0,
    },
    price: {
      price: { type: mongoose.Schema.Types.Number, default: 5900 },
      discount_everyone: { type: mongoose.Schema.Types.Number, default: 0 },
      minimum_price: { type: mongoose.Schema.Types.Number, default: 0 },
      bulk_discount: { type: mongoose.Schema.Types.Number, default: 0 },
      discount_returning_customer: { type: mongoose.Schema.Types.Number, default: 0 },
    },
    _cover: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Illustration',
      required: false,
      default: null,
    },
    university: {
      id: { type: mongoose.Schema.Types.String },
      name: { type: mongoose.Schema.Types.String },
      url_slug: { type: mongoose.Schema.Types.String },
    },
    columns: { type: [ColumnSchema], required: true, default: [] },
    stats: {
      chapterCount: { type: mongoose.Schema.Types.Number, default: 0 },
      examCount: { type: mongoose.Schema.Types.Number, default: 0 },
      examAssignmentCount: { type: mongoose.Schema.Types.Number, default: 0 },
      examAssignmentsByTopicCount: { type: mongoose.Schema.Types.Number, default: 0 },
      examAsignmentsSectionCount: { type: mongoose.Schema.Types.Number, default: 0 },
      examAsignmentsByTopicSectionCount: { type: mongoose.Schema.Types.Number, default: 0 },
      additionalTasksSectionCount: { type: mongoose.Schema.Types.Number, default: 0 },
      additionalTasksCount: { type: mongoose.Schema.Types.Number, default: 0 },
      progressAverageUnderstood: { type: mongoose.Schema.Types.Number, default: 0 },
      progressAverageProcessed: { type: mongoose.Schema.Types.Number, default: 0 },
    },
  },
  {
    strict: false,
    timestamps: true,
  },
);

export const coursesProvider = [
  {
    provide: COURSE_MODEL,
    useFactory: (connection: Connection) => connection.model(COURSE_MODEL, CourseSchema),
    inject: [DB_CONNECT_INJECTION],
  },
];
