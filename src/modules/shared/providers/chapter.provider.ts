import mongoose, { Connection } from 'mongoose';
import { DB_CONNECT_INJECTION, CHAPTER_MODEL } from '../../constants/db';
import { ChapterDisplayMode, ChapterPriority } from '../../../../shared/src';

export const ChapterSchema = new mongoose.Schema(
  {
    title: { type: String, required: true, unique: false },
    sections: { type: [] },
    displayMode: {
      type: ChapterDisplayMode,
      default: 1,
    },
    priority: { type: ChapterPriority, required: false, default: 0 },
    url_slug: { type: String, required: true, unique: true },
    course: { type: mongoose.Schema.Types.ObjectId, ref: 'Chapter' },
    path: { type: String },
    inPreview: { type: Boolean, required: false, default: 0 },
    order: { type: Number, default: 0, required: true },
    orderString: { type: String, default: 0, required: true },
    columnId: { type: String, required: true, default: '0' },
    subChapterCount: { type: Number, default: 0, required: true },
    hasSections: { type: Boolean, default: true, required: false },
  },
  {
    _id: true,
    timestamps: true,
  },
);

export const chapterProvider = [
  {
    provide: CHAPTER_MODEL,
    useFactory: (connection: Connection) => connection.model(CHAPTER_MODEL, ChapterSchema),
    inject: [DB_CONNECT_INJECTION],
  },
];
