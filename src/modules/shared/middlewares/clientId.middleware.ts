import { Injectable, NestMiddleware, ForbiddenException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { RequestClaims } from './user-claim.middleware';
import { ClientId } from '../../../../shared/src';

@Injectable()
export class ClientIdMiddleware implements NestMiddleware {
  constructor() {}

  private clientIdHeader = 'ClientId';

  async use(req: RequestClaims, res: Response, next: (e?: Error) => void): Promise<void> {
    const clientIdHeader: number = <number>req.headers[this.clientIdHeader.toLowerCase()];

    if (clientIdHeader in ClientId) {
      req.clientId = clientIdHeader as ClientId;
      next();
    } else {
      next(new ForbiddenException('Forbidden', 'You do not have access to this resource'));
    }
  }
}

