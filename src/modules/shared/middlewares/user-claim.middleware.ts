import {
  Injectable,
  NestMiddleware,
  ForbiddenException,
  Inject,
  UnauthorizedException,
} from '@nestjs/common';
import { get, chain } from 'lodash';
import { Model } from 'mongoose';
import * as jwt from 'jsonwebtoken';
import { UserData } from '../types/user.types';
import { PROFILE_MODEL } from '../../constants/db';
import { Mailer } from '../services/mailer.service';
import { ClientId } from '../../../../shared/src';
export interface RequestClaims extends Request {
  user: UserData;
  clientId: ClientId;
}

@Injectable()
export class UserClaimMiddleware implements NestMiddleware {
  private userAuthHeader = 'Authorization';
  private userAuthSchema = 'Bearer';
  constructor(
    @Inject(PROFILE_MODEL) private profileModel: Model<any>,
    private mailService: Mailer,
  ) {}
  async use(req: RequestClaims, res: Response, next: (e?: Error) => void): Promise<void> {
    const authHeader: string = <string>req.headers[this.userAuthHeader.toLowerCase()];
    const authToken: string = authHeader
      ? <string>authHeader.split(`${this.userAuthSchema} `)[1]
      : undefined;
    if (authToken) {
      try {
        const userClaimsResult = jwt.decode(authToken) as UserData;
        const profile = await this.profileModel.findOne({ user_id: get(userClaimsResult, 'sub') });
        await this.lastJWTCheck(userClaimsResult, profile, req.headers['clientid']);
        req.user = userClaimsResult;
        next();
      } catch (e) {
        return e
          ? next(e)
          : next(new ForbiddenException('Forbidden', 'You do not have access to this resource'));
      }
    } else {
      return next(new ForbiddenException('Forbidden', 'You do not have access to this resource'));
    }
  }

  private async lastJWTCheck(auth0user, profile, clientid: ClientId) {
    const profileLastLogin = get(profile, 'last_login');
    const authLastLogin = get(auth0user, 'iat');
    const forcedLogouts = get(profile, 'forcedLogouts', []);
    const isAdmin = chain(auth0user)
      .get('https://ecoreps.de/roles', [])
      .includes('admin')
      .value();
    if (!profileLastLogin) {
      if (profile) {
        profile.last_login = authLastLogin;
        await profile.save();
      }
      return true;
    } else {
      if (isAdmin) {
        return true;
      }
      if (profileLastLogin > authLastLogin) {
        const now = new Date().getTime();
        const yesterday = now - 3600 * 1000;
        profile.forcedLogouts = forcedLogouts.filter(t => t > yesterday);
        profile.forcedLogouts.push(now);
        const forcedLength = profile.forcedLogouts.length;
        if (forcedLength >= 5 && forcedLength % 5 === 0) {
          const email = get(auth0user, 'email');
          await this.mailService.notifyBlockedLogins(email, clientid);
        }
        await profile.save();
        throw new UnauthorizedException('Unauthorize');
      } else if (profileLastLogin === authLastLogin) {
        return true;
      } else if (profileLastLogin < authLastLogin) {
        profile.last_login = authLastLogin;
        await profile.save();
        return true;
      }
    }
  }
}
