FROM node:12

RUN npm uninstall -g yarn
RUN npm uninstall yarn
RUN npm install yarn
WORKDIR /app
COPY ./package.json .
COPY ./services/ecoreps.be.srv.profile/tsconfig.json ./services/ecoreps.be.srv.profile/tsconfig.json
COPY ./services/ecoreps.be.srv.profile/tsconfig.build.json ./services/ecoreps.be.srv.profile/tsconfig.build.json
COPY ./packages/ecoreps.be.lib.shared ./packages/ecoreps.be.lib.shared
COPY ./services/ecoreps.be.srv.profile ./services/ecoreps.be.srv.profile


RUN yarn;

RUN ./node_modules/.bin/tsc -p packages/ecoreps.be.lib.shared/tsconfig.build.json
RUN ./node_modules/.bin/tsc -p services/ecoreps.be.srv.profile/tsconfig.build.json

RUN yarn install --non-interactive --cache-folder ./ycache; rm -rf ./ycache; 

ENV NODE_ENV="staging"

EXPOSE 5002

CMD yarn --cwd ./services/ecoreps.be.srv.profile start:staging


