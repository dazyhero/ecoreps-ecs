import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import MongoClient from 'mongodb';

const dbURL = process.env.MONGO_URL;

const mockedJWT =
  'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik1rTXdSVE5EUlRKRE1qZEJPVEl3TlRVM1F6QTJPRUl6TnpNMlJUTXdNRGhHUlRZMlJEVXhPUSJ9.eyJodHRwczovL2Vjb3JlcHMuZGUvcm9sZXMiOlsiYWRtaW4iXSwiaHR0cHM6Ly9lY29yZXBzLmRlL3B1cmNoYXNlZF9jb3Vyc2VzIjpbXSwiZ2l2ZW5fbmFtZSI6ItCc0LDQutGB0LjQvCIsImZhbWlseV9uYW1lIjoi0JfQsNC60YjQtdCy0YHQutC40LkiLCJuaWNrbmFtZSI6ItCc0LDQutGB0LjQvCDQl9Cw0LrRiNC10LLRgdC60LjQuSIsIm5hbWUiOiLQnNCw0LrRgdC40Lwg0JfQsNC60YjQtdCy0YHQutC40LkiLCJwaWN0dXJlIjoiaHR0cHM6Ly9wbGF0Zm9ybS1sb29rYXNpZGUuZmJzYnguY29tL3BsYXRmb3JtL3Byb2ZpbGVwaWMvP2FzaWQ9Mjk5OTY4NTk4NjgxODA4NSZoZWlnaHQ9NTAmd2lkdGg9NTAmZXh0PTE2MDI4ODYzNzEmaGFzaD1BZVR0Y2N3d1ZndUUxaW5jIiwidXBkYXRlZF9hdCI6IjIwMjAtMDktMTZUMjI6MTI6NTEuOTA4WiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2Vjb3JlcHMtZGV2LmV1LmF1dGgwLmNvbS8iLCJzdWIiOiJmYWNlYm9va3wyOTk5Njg1OTg2ODE4MDg1IiwiYXVkIjoiUUM0T2ZVajhrT1JqamU3c05lYUg1RVFQY2VFQ0phNEYiLCJpYXQiOjE2MDAyOTQzNzIsImV4cCI6MTYwMDMzMDM3MiwiYXRfaGFzaCI6Il9aVG9JYVFOWkNTQTRtZDRVTTdzLUEiLCJub25jZSI6Ik9CUTlYfkxDS25lR3FJRVJWS1FwOHEtUFV3YkZ2cENQIn0.0tqoa1XabN34kJ9MiGCoSTT69Phwz67CSHZicEMq4mZKdPBp7DNe5a1O6ptnmr411-3O0111DpUrvmP4AjybjLBMqh7zKbwIRGbB01k_n3xvEUKJbqAva9myYuD8LfZuKzK86AngMRaxRWxZTzdT3TERUPFjAkzz5VNnRrs-AF7detXJmMgE8kHtsAoQqdslOkNtonefnmOUrjlvg4gBj53eEseUDY6KqijL8xhYegUwwmzLUILzgH-nVlsRENORcVt-QxRDmutLj9XEVNHnQD8jcMAjqFYVNHcck9BlNZ3Rf5kWUilSRJ2cH7XjhXFtcGkyXGeJDNPwjsW6QK2n5g';

const mockedProfile = {
  at_hash: '_ZToIaQNZCSA4md4UM7s-A',
  aud: 'QC4OfUj8kORjje7sNeaH5EQPceECJa4F',
  email_verified: true,
  exp: 1600330372,
  family_name: 'Закшевский',
  given_name: 'Максим',
  'https://ecoreps.de/purchased_courses': [],
  'https://ecoreps.de/roles': ['admin'],
  iat: 1600294372,
  iss: 'https://ecoreps-dev.eu.auth0.com/',
  name: 'Максим Закшевский',
  nickname: 'Максим Закшевский',
  nonce: 'OBQ9X~LCKneGqIERVKQp8q-PUwbFvpCP',
  picture:
    'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2999685986818085&height=50&width=50&ext=1602886371&hash=AeTtccwwVguE1inc',
  sub: 'facebook|2999685986818085',
  updated_at: '2020-09-16T22:12:51.908Z',
};

describe('Profile Controller', () => {
  let app: INestApplication;
  let db;
  let connection;

  beforeAll(async () => {
    connection = await MongoClient.connect(dbURL);
    db = await connection.db();
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  it('GET profile', () => {
    return request(app.getHttpServer())
      .get('/profile')
      .set('Authorization', mockedJWT)
      .expect(200)
      .expect(data => {
        const fromJSON = JSON.parse(data.text);
        expect(fromJSON['_decodedToken']).toEqual(mockedProfile);
      });
  });

  it('GET session', () => {
    return request(app.getHttpServer())
      .get('/profile/session')
      .set('Authorization', mockedJWT)
      .set('ClientId', '100')
      .expect(200)
      .expect(data => {
        const fromJSON = JSON.parse(data.text);
        expect(fromJSON).toEqual(true);
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
