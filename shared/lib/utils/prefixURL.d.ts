export interface IObjectKeyReplacement {
    key: string;
    target: string;
    isArray?: boolean;
}
export declare function prefixUrlForObject(object: any, urlMapper: (value: string) => string, replacements: IObjectKeyReplacement[]): any;
