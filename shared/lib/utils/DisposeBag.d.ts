import { Subscription } from "rxjs";
export default class DisposeBag {
    private _subscriptions;
    add(subscription: Subscription): void;
    addSubscriptions(subscriptions: Subscription[]): void;
    dispose(): void;
}
