"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.courseByIdLOrSlugQuery = exports.isNumericId = exports.isValidObjectId = void 0;
function isValidObjectId(input) {
    let regex = new RegExp(/^[a-fA-F0-9]{24}$/);
    return regex.test(input);
}
exports.isValidObjectId = isValidObjectId;
function isNumericId(input) {
    let numericRepresentation = Number(input);
    return !isNaN(numericRepresentation) && numericRepresentation % 1 == 0;
}
exports.isNumericId = isNumericId;
function courseByIdLOrSlugQuery(input, includeDeleted = false) {
    let deletedQuery = (includeDeleted) ? {} : { $or: [{ deleted: false }, { deleted: { $exists: false } }] };
    if (isNumericId(input)) {
        console.log("IS NUMERIC ID:" + isNumericId(input));
        return Object.assign(Object.assign({}, deletedQuery), { externalId: input });
    }
    else if (isValidObjectId(input)) {
        console.log("is valid object ID: " + input);
        return Object.assign(Object.assign({}, deletedQuery), { _id: input });
    }
    else {
        console.log("is valid url slug?: " + input);
        return Object.assign(Object.assign({}, deletedQuery), { url_slug: input });
    }
}
exports.courseByIdLOrSlugQuery = courseByIdLOrSlugQuery;
//# sourceMappingURL=ObjectId.js.map