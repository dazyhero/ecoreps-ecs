import { ChapterDisplayMode, ChapterType, IChapter, ChapterPriority } from '../model/chapter';
import { IColumn } from '../model/course';
export declare const sortDeleted: (chapter: IChapter[], predicate?: any) => any[];
export declare const allChapterTypes: ChapterType[];
export declare const allChapterPriorities: ChapterPriority[];
export declare const allChapterDisplayModes: ChapterDisplayMode[];
interface TreeChapter {
    chapter: IChapter;
    pathArray: string[];
}
export declare function orderStringToFloat(str: string): number;
export declare function chapterSortFunction(chapter1: any, chapter2: any): 1 | -1;
export declare function chapter_tree_from_paths2(chapters: IChapter[], root: string, toObjects?: boolean): IChapter[];
export declare function chapter_tree_from_paths(chapters: IChapter[] | TreeChapter[], root: string): IChapter[];
export declare function displayModeForChildrenAfterModification(parentPrevious: ChapterDisplayMode, parentNew: ChapterDisplayMode): ChapterDisplayMode.hierachical | ChapterDisplayMode.includedInParent;
export declare function _chapter_tree_from_paths(chapters: IChapter[] | TreeChapter[], root: string, arrayMapped?: boolean): TreeChapter[];
export declare function flatten(chapters: IChapter[], startIndex?: ChapterIndex): IChapter[];
export declare function flattenIds(chapters: IChapter[], startIndex?: ChapterIndex): IChapter[];
export declare function findSurroundingChapter2(currentChapterId: string, chapters: IChapter[], backward: boolean): IChapter;
export declare function patternForSubChapters(chapter: IChapter): RegExp;
export declare function hasSections(chapter: IChapter): boolean;
export declare function assignNavigationLinksInChapters2(sortedChapters: IChapter[]): IChapter[];
export declare function assignNavigationLinksInChapters(chapters: IChapter[]): IChapter[];
export declare function assignFullNames(chapters: IChapter[], startIndex: ChapterIndex, columns: IColumn[]): IChapter[];
export declare function findItemInSlugItems<T>(itemId: string, collection: T[]): T;
export interface PathComponent {
    key: string;
    _id: string;
}
export declare function findChapterContainingDisplay(chapter: IChapter, chapters: IChapter[]): IChapter;
export declare function findChapterByIdInChapters(chapterId: string, chapters: IChapter[], recursive?: boolean): IChapter;
export declare function findChapterContaining(chapterId: string, chapters: IChapter[]): IChapter;
export declare function applyHierarchical(chapters: IChapter[], closure: (parent: IChapter, child: IChapter) => void, parent?: IChapter): void;
export declare function findParentChapterOf(chapter: IChapter, chapters: IChapter[]): IChapter;
export declare class ChapterIndex {
    private values;
    get description(): string;
    get level(): number;
    static startIndex(): ChapterIndex;
    indexAtLevel(level: number): number;
    contains(maybeChild: ChapterIndex): boolean;
    private beginAt;
    enhanced: (level: number, by?: number) => ChapterIndex;
    get nextSibling(): ChapterIndex;
    get previousSibling(): ChapterIndex;
    get nextChild(): ChapterIndex;
    branched: () => ChapterIndex;
    get levels(): number[];
    constructor(values: number[]);
}
export {};
