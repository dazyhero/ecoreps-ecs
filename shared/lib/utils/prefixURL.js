"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.prefixUrlForObject = void 0;
function prefixUrlForObject(object, urlMapper, replacements) {
    console.log("calls uni replace with object: " + JSON.stringify(object));
    for (let r of replacements) {
        var value = recursiveGet(object, r.key.split("."));
        let newValue = (r.isArray) ? value.map(i => urlMapper(i)) : urlMapper(value);
        recursiveSet(object, r.target.split("."), newValue);
    }
    return object;
}
exports.prefixUrlForObject = prefixUrlForObject;
function recursiveGet(object, nestedKeys) {
    console.log("calls uni replace get with object: " + JSON.stringify(object) + " and keys: " + JSON.stringify(nestedKeys));
    if (!object) {
        return null;
    }
    if (nestedKeys.length < 1) {
        return null;
    }
    let firstKey = nestedKeys[0];
    nestedKeys.shift();
    let value = (nestedKeys.length == 0) ? object[firstKey] : recursiveGet(object[firstKey], nestedKeys);
    return value;
}
function recursiveSet(object, nestedKeys, value) {
    if (nestedKeys.length < 1) {
        return null;
    }
    let firstKey = nestedKeys[0];
    nestedKeys.shift();
    let newObject = (nestedKeys.length == 0) ? value : recursiveSet(object[firstKey], nestedKeys, value);
    object[firstKey] = newObject;
    return object;
}
//# sourceMappingURL=prefixURL.js.map