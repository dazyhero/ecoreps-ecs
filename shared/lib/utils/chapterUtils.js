"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChapterIndex = exports.findParentChapterOf = exports.applyHierarchical = exports.findChapterContaining = exports.findChapterByIdInChapters = exports.findChapterContainingDisplay = exports.findItemInSlugItems = exports.assignFullNames = exports.assignNavigationLinksInChapters = exports.assignNavigationLinksInChapters2 = exports.hasSections = exports.patternForSubChapters = exports.findSurroundingChapter2 = exports.flattenIds = exports.flatten = exports._chapter_tree_from_paths = exports.displayModeForChildrenAfterModification = exports.chapter_tree_from_paths = exports.chapter_tree_from_paths2 = exports.chapterSortFunction = exports.orderStringToFloat = exports.allChapterDisplayModes = exports.allChapterPriorities = exports.allChapterTypes = exports.sortDeleted = void 0;
const chapter_1 = require("../model/chapter");
const ObjectId_1 = require("./ObjectId");
exports.sortDeleted = (chapter, predicate) => {
    return !chapter
        ? null
        : chapter.reduce((list, entry) => {
            let clone = null;
            if (predicate(entry)) {
                clone = Object.assign({}, entry);
            }
            else if (entry.subchapters !== null) {
                let subchapters = exports.sortDeleted(entry.subchapters, predicate);
                if (subchapters.length > 0) {
                    clone = Object.assign(Object.assign({}, entry), { subchapters });
                }
            }
            clone && list.push(clone);
            return list;
        }, []);
};
exports.allChapterTypes = [
    chapter_1.ChapterType.summaryChapter,
    chapter_1.ChapterType.exam,
    chapter_1.ChapterType.examAssignment,
    chapter_1.ChapterType.examByTopicChapter,
    chapter_1.ChapterType.additionalTasks,
];
exports.allChapterPriorities = [chapter_1.ChapterPriority.normal, chapter_1.ChapterPriority.highlighted];
exports.allChapterDisplayModes = [
    chapter_1.ChapterDisplayMode.hierachical,
    chapter_1.ChapterDisplayMode.includeChildren,
    chapter_1.ChapterDisplayMode.includedInParent,
];
function orderStringToFloat(str) {
    let splitString = str.split(',');
    if (splitString.length == 0) {
        return 0;
    }
    var floatString = splitString[0];
    if (splitString.length == 1) {
        return parseFloat(floatString);
    }
    floatString =
        floatString +
            '.' +
            splitString
                .slice(1, splitString.length)
                .map(s => s.split('.').join(''))
                .join('');
    return parseFloat(floatString);
}
exports.orderStringToFloat = orderStringToFloat;
function chapterSortFunction(chapter1, chapter2) {
    let a = chapter1.orderString;
    let b = chapter2.orderString;
    let aElements = a.split(',').map(i => {
        return parseFloat(i);
    });
    let bElements = b.split(',').map(i => {
        return parseFloat(i);
    });
    let count = Math.max(aElements.length, bElements.length);
    var bBiggerThanA = false;
    for (let i = 0; i < count; i++) {
        if (i < aElements.length && i < bElements.length) {
            if (aElements[i] == bElements[i]) {
            }
            else {
                bBiggerThanA = bElements[i] > aElements[i];
                break;
            }
        }
        else {
            bBiggerThanA = bElements.length > aElements.length;
        }
    }
    return bBiggerThanA ? -1 : 1;
}
exports.chapterSortFunction = chapterSortFunction;
function chapter_tree_from_paths2(chapters, root, toObjects = false) {
    var rootLevel = null;
    let treeChapters = chapters
        .map(c => (toObjects ? c.toObject() : c))
        .map((chapter) => {
        let path = chapter.path.split(',');
        if (null === rootLevel) {
            let index = path.indexOf(root);
            if (index < -1) {
                return null;
            }
            rootLevel = index + 1;
        }
        path.splice(0, rootLevel);
        chapter.subchapters = [];
        if (toObjects) {
            chapter.subchapters = [];
        }
        else {
            chapter.set('subchapters', [], { strict: false });
        }
        return {
            chapter: chapter,
            pathArray: path,
        };
    })
        .filter(Boolean);
    var subCache = {};
    var titleSubCache = {};
    let hasSubs = [];
    for (let tc of treeChapters) {
        if (tc.pathArray.length > 0) {
            let treeParent = treeChapters
                .filter(Boolean)
                .find(chapter => chapter.chapter._id.toString() == tc.pathArray[tc.pathArray.length - 1]);
            if (!treeParent) {
                continue;
            }
            let parent = treeParent.chapter;
            subCache[parent._id.toString()] = subCache[parent._id.toString()] || [];
            subCache[parent._id.toString()].push(tc.chapter);
            titleSubCache[parent.title.toString()] = titleSubCache[parent.title.toString()] || [];
            titleSubCache[parent.title.toString()].push(tc.chapter.title);
            let sortedSubCache = subCache[parent._id.toString()].sort(chapterSortFunction);
            if (hasSubs.findIndex(val => val._id.toString() == parent._id.toString()) == -1) {
                hasSubs.push(parent);
            }
        }
    }
    for (let hasSub of hasSubs) {
        let sortedSubs = subCache[hasSub._id.toString()].sort(chapterSortFunction);
        if (toObjects) {
            hasSub.subchapters = sortedSubs;
        }
        else {
            hasSub.set('subchapters', sortedSubs, { strict: false });
        }
    }
    let level1 = treeChapters
        .filter(tc => tc.pathArray.length == 0)
        .map(chap => {
        return {
            title: chap.chapter.title,
            subchapters: chap.chapter.subchapters.map(sc => {
                return {
                    title: sc.title,
                    subchapters: sc.subchapters.map(ssc => ssc.title),
                };
            }),
        };
    });
    let result = treeChapters
        .filter(tc => tc.pathArray.length == 0)
        .map(tc => tc.chapter)
        .sort(chapterSortFunction);
    applyHierarchical(result, (parent, child) => {
        let hs = hasSubs.find(h => child._id.toString() == h._id.toString());
        if (hs) {
            child.subchapters = hs.subchapters;
        }
    });
    return result;
}
exports.chapter_tree_from_paths2 = chapter_tree_from_paths2;
function chapter_tree_from_paths(chapters, root) {
    return _chapter_tree_from_paths(chapters, root).map(c => c.chapter);
}
exports.chapter_tree_from_paths = chapter_tree_from_paths;
function displayModeForChildrenAfterModification(parentPrevious, parentNew) {
    if (parentPrevious == parentNew) {
        return null;
    }
    else if (parentPrevious != chapter_1.ChapterDisplayMode.hierachical) {
        return chapter_1.ChapterDisplayMode.hierachical;
    }
    else {
        return chapter_1.ChapterDisplayMode.includedInParent;
    }
}
exports.displayModeForChildrenAfterModification = displayModeForChildrenAfterModification;
function _chapter_tree_from_paths(chapters, root, arrayMapped = false) {
    let arrayMap = arrayMapped
        ? chapters.map(treechapter => {
            treechapter.pathArray.splice(0, 1);
            return treechapter;
        })
        : chapters
            .map(chapter => {
            let path = chapter.path.split(',');
            if (!path || path.length == 0 || path[0] != root) {
                return null;
            }
            path.splice(0, 1);
            return {
                chapter: chapter,
                pathArray: path,
            };
        })
            .filter(Boolean);
    if (arrayMapped) {
        return chapters;
    }
    let roots = arrayMap.filter(c => c.pathArray.length == 0);
    roots = roots.map(rootTreeChapter => {
        let rootChapter = rootTreeChapter.chapter;
        let rootKey = rootChapter.url_slug;
        let filtered_sub = arrayMap.filter(chapter => {
            return chapter.pathArray.length > 0 && rootKey == chapter.pathArray[0];
        });
        let subchapters = _chapter_tree_from_paths(filtered_sub, rootKey, true);
        rootChapter.set('subchapters', subchapters.map(c => c.chapter), { strict: false });
        return {
            chapter: rootChapter,
            pathArray: rootTreeChapter.pathArray,
        };
    });
    return roots;
}
exports._chapter_tree_from_paths = _chapter_tree_from_paths;
function flatten(chapters, startIndex = null) {
    if (!chapters || chapters.length == 0) {
        return [];
    }
    let flattened = [];
    var index = startIndex;
    for (let chap of chapters) {
        var branched = null;
        if (index) {
            chap.index = index;
            branched = index.branched();
            var _subindex = branched;
            if (chap.subchapters && chap.subchapters.length > 0) {
                for (let sub of chap.subchapters) {
                    sub.index = _subindex;
                    _subindex = _subindex.nextSibling;
                }
            }
        }
        flattened.push(chap);
        if (chap.subchapters != undefined && chap.subchapters != null && chap.subchapters.length > 0) {
            flattened = flattened.concat(flatten(chap.subchapters, branched));
        }
        if (index) {
            index = index.nextSibling;
        }
    }
    return flattened;
}
exports.flatten = flatten;
function flattenIds(chapters, startIndex = null) {
    var index = startIndex || ChapterIndex.startIndex();
    if (chapters == undefined || chapters.length == 0) {
        return [];
    }
    let flattened = [];
    for (let chap of chapters) {
        chap.index = index;
        var containsDisplay = [];
        if (!hasSections(chap) &&
            chap.displayMode == chapter_1.ChapterDisplayMode.hierachical &&
            chap.subchapters.length > 0) {
            containsDisplay.push(chap.subchapters[0]._id);
        }
        let flattenedSubChapters = flattenIds(chap.subchapters, index.branched());
        if (chap.displayMode != chapter_1.ChapterDisplayMode.hierachical ||
            (chap.subchapters.length > 0 &&
                chap.subchapters[0].displayMode != chapter_1.ChapterDisplayMode.hierachical)) {
            let extra = flattenedSubChapters.map(sc => sc._id);
            containsDisplay = containsDisplay.concat(extra);
            let extra2 = flattenedSubChapters.map(sc => sc.containsDisplay);
            for (let cd of extra2) {
                containsDisplay = containsDisplay.concat(cd);
            }
        }
        chap.containsDisplay = containsDisplay;
        if (chap.displayMode != chapter_1.ChapterDisplayMode.hierachical) {
            applyHierarchical(flattenedSubChapters, (p, c) => {
                c.containsDisplay = containsDisplay;
            });
        }
        flattened.push(chap);
        flattened = flattened.concat(flattenedSubChapters);
        index = index.nextSibling;
    }
    return flattened;
}
exports.flattenIds = flattenIds;
function findSurroundingChapter2(currentChapterId, chapters, backward) {
    console.log('looking for surrounding: ' + currentChapterId);
    let chapter = findChapterByIdInChapters(currentChapterId, chapters);
    let flattened = flattenIds(chapters);
    let index = flattened.findIndex(c => {
        return c._id == currentChapterId || c.url_slug == currentChapterId;
    });
    if (index == -1) {
        return null;
    }
    let split = backward ? flattened.slice(0, index).reverse() : flattened.slice(index + 1);
    if (split.length == 0) {
        return null;
    }
    let firstNonIncluding = split.findIndex(splitc => {
        if (splitc) {
        }
        return (splitc.containsDisplay.findIndex(v => v == chapter._id) == -1 &&
            flattened[index].containsDisplay.findIndex(v => v == splitc._id) == -1);
    });
    if (firstNonIncluding >= 0) {
        return split[firstNonIncluding];
    }
    else {
        console.log('found no surrounding: ' + split[firstNonIncluding].url_slug);
        return null;
    }
}
exports.findSurroundingChapter2 = findSurroundingChapter2;
function patternForSubChapters(chapter) {
    let matcher = new RegExp(chapter._id.toString() + '(,|$)', 'i');
    return matcher;
}
exports.patternForSubChapters = patternForSubChapters;
function hasSections(chapter) {
    return chapter.hasSections || (chapter.sections && chapter.sections.length > 0);
}
exports.hasSections = hasSections;
function assignNavigationLinksInChapters2(sortedChapters) {
    let flattened = sortedChapters;
    var needsNextIndizes = [];
    var lastFullPageIndex = -1;
    for (let i = 0; i < flattened.length; i++) {
        let chapter = flattened[i];
        flattened[i].set('previous', lastFullPageIndex >= 0 ? flattened[lastFullPageIndex].url_slug : null, { strict: false });
        let isPage = (chapter.displayMode == chapter_1.ChapterDisplayMode.hierachical && hasSections(chapter)) ||
            chapter.displayMode == chapter_1.ChapterDisplayMode.includeChildren;
        if (isPage) {
            lastFullPageIndex = i;
            for (let nn of needsNextIndizes) {
                flattened[nn].next = chapter.url_slug;
                flattened[nn].set('next', chapter.url_slug, { strict: false });
            }
            needsNextIndizes = [];
        }
        needsNextIndizes.push(i);
    }
    return sortedChapters;
}
exports.assignNavigationLinksInChapters2 = assignNavigationLinksInChapters2;
function assignNavigationLinksInChapters(chapters) {
    if (!chapters || chapters.length == 0) {
        return [];
    }
    let flattened = flatten(chapters, ChapterIndex.startIndex());
    var needsNextIndizes = [];
    var lastFullPageIndex = -1;
    for (let i = 0; i < flattened.length; i++) {
        let chapter = flattened[i];
        flattened[i].set('previous', lastFullPageIndex >= 0 ? flattened[lastFullPageIndex].url_slug : null, { strict: false });
        let isPage = (chapter.displayMode == chapter_1.ChapterDisplayMode.hierachical && hasSections(chapter)) ||
            chapter.displayMode == chapter_1.ChapterDisplayMode.includeChildren;
        if (isPage) {
            lastFullPageIndex = i;
            for (let nn of needsNextIndizes) {
                flattened[nn].next = chapter.url_slug;
                flattened[nn].set('next', chapter.url_slug, { strict: false });
            }
            needsNextIndizes = [];
        }
        needsNextIndizes.push(i);
    }
    applyHierarchical(chapters, (p, chapter) => {
        let f_chapter = flattened.find(c => {
            return c._id == chapter._id;
        });
        chapter.next = f_chapter.next;
        chapter.previous = f_chapter.previous;
    });
    return chapters;
}
exports.assignNavigationLinksInChapters = assignNavigationLinksInChapters;
function assignFullNames(chapters, startIndex, columns) {
    var index = startIndex;
    for (let chapter of chapters) {
        var branched = null;
        let column = columns.find(c => c.id.toString() == chapter.columnId.toString());
        if (!column) {
            alert('cant find col with id: ' + chapter.columnId + 'of chapter: ' + JSON.stringify(chapter));
        }
        let typeInfo = new chapter_1.ChapterTypeInfo(column);
        if (index) {
            chapter.index = index;
            branched = index.branched();
        }
        else if (chapter.orderString) {
            chapter.index = new ChapterIndex(chapter.orderString.split(',').map(i => parseInt(i)));
        }
        chapter.fullTitle = typeInfo.showChapterIndizes
            ? chapter.index.description + ' ' + chapter.title
            : chapter.title;
        if (chapter.subchapters != undefined && chapter.subchapters.length > 0) {
            chapter.subchapters = assignFullNames(chapter.subchapters, index ? branched : null, columns);
        }
        if (index) {
            index = index.nextSibling;
        }
    }
    return chapters;
}
exports.assignFullNames = assignFullNames;
function findItemInSlugItems(itemId, collection) {
    let field = ObjectId_1.isValidObjectId(itemId) ? '_id' : 'url_slug';
    if (collection.length == 0) {
        return null;
    }
    for (let item of collection) {
        if (item[field] == itemId) {
            return item;
        }
    }
    return null;
}
exports.findItemInSlugItems = findItemInSlugItems;
function findChapterContainingDisplay(chapter, chapters) {
    if (chapter.displayMode == chapter_1.ChapterDisplayMode.includedInParent) {
        let parent = findParentChapterOf(chapter, chapters);
        if (parent) {
            return findChapterContainingDisplay(parent, chapters);
        }
        else {
            console.error('cant find parent chapter');
            console.error([chapter, flatten(chapters)]);
        }
    }
    return chapter;
}
exports.findChapterContainingDisplay = findChapterContainingDisplay;
function findChapterByIdInChapters(chapterId, chapters, recursive = true) {
    let field = ObjectId_1.isValidObjectId(chapterId) ? '_id' : 'url_slug';
    if (chapters.length == 0) {
        return null;
    }
    for (let chapter of chapters) {
        if (chapter[field] == chapterId) {
            return chapter;
        }
        else if (recursive && chapter.subchapters) {
            let subChapter = findChapterByIdInChapters(chapterId, chapter.subchapters, true);
            if (subChapter != null && !(subChapter == undefined)) {
                return subChapter;
            }
        }
    }
    return null;
}
exports.findChapterByIdInChapters = findChapterByIdInChapters;
function findChapterContaining(chapterId, chapters) {
    let field = ObjectId_1.isValidObjectId(chapterId) ? '_id' : 'url_slug';
    if (chapters.length == 0 || chapters.findIndex(value => value[field] == chapterId) > 0) {
        return null;
    }
    for (let chapter of chapters) {
        let subChapter = findChapterByIdInChapters(chapterId, chapter.subchapters, false);
        if (subChapter != null && subChapter !== undefined) {
            return chapter;
        }
        else {
            let chap = findChapterContaining(chapterId, chapter.subchapters);
            if (chap) {
                return chap;
            }
        }
    }
    return null;
}
exports.findChapterContaining = findChapterContaining;
function applyHierarchical(chapters, closure, parent = null) {
    if (!chapters) {
        return;
    }
    for (let chap of chapters) {
        closure(parent, chap);
        applyHierarchical(chap.subchapters, closure, chap);
    }
}
exports.applyHierarchical = applyHierarchical;
function findParentChapterOf(chapter, chapters) {
    let flattened = flatten(chapters);
    let parent = flattened.find(chap => {
        return chap.subchapters.find(sub => sub._id == chapter._id) !== undefined;
    });
    return parent;
}
exports.findParentChapterOf = findParentChapterOf;
class ChapterIndex {
    constructor(values) {
        this.values = values;
        this.beginAt = 1;
        this.enhanced = (level, by = 1) => {
            if (level > this.level || level < 0) {
                throw new Error('Tried to enhanceindex at level that is too high or too low ');
            }
            var resultValues = [];
            for (let i in this.values) {
                if (parseInt(i) === level) {
                    resultValues[i] = this.values[i] + by;
                }
                else {
                    resultValues[i] = this.values[i];
                }
            }
            return new ChapterIndex(resultValues);
        };
        this.branched = () => {
            var resultValues = [];
            for (let i in this.values) {
                resultValues[i] = this.values[i];
            }
            resultValues[this.values.length] = this.beginAt;
            return new ChapterIndex(resultValues);
        };
    }
    get description() {
        return this.values.join('.');
    }
    get level() {
        return this.values.length - 1;
    }
    static startIndex() {
        return new ChapterIndex([1]);
    }
    indexAtLevel(level) {
        return this.values[level];
    }
    contains(maybeChild) {
        if (maybeChild.level < this.level) {
            return false;
        }
        for (let i in this.values) {
            if (this.indexAtLevel(parseInt(i)) != maybeChild.indexAtLevel(parseInt(i))) {
                return false;
            }
        }
        return true;
    }
    get nextSibling() {
        return this.enhanced(this.level, 1);
    }
    get previousSibling() {
        let enh = this.enhanced(this.level, -1);
        return enh;
    }
    get nextChild() {
        return this.branched();
    }
    get levels() {
        return this.values;
    }
}
exports.ChapterIndex = ChapterIndex;
//# sourceMappingURL=chapterUtils.js.map