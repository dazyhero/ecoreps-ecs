export declare function isValidObjectId(input: string): boolean;
export declare function isNumericId(input: string): boolean;
export declare function courseByIdLOrSlugQuery(input: string, includeDeleted?: boolean): any;
