"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DisposeBag {
    constructor() {
        this._subscriptions = [];
    }
    add(subscription) {
        this._subscriptions.push(subscription);
    }
    addSubscriptions(subscriptions) {
        this._subscriptions = this._subscriptions.concat(subscriptions);
    }
    dispose() {
        for (let sub of this._subscriptions) {
            sub.unsubscribe();
        }
        this._subscriptions = [];
    }
}
exports.default = DisposeBag;
//# sourceMappingURL=DisposeBag.js.map