"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.combineLatestObservablesWithTriggeringIndex = void 0;
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
function combineLatestObservablesWithTriggeringIndex(observables) {
    let timedObservables = observables.map(obs => obs.pipe(operators_1.map(val => { return { value: val, date: new Date().getTime() }; })));
    let combined = rxjs_1.combineLatest(timedObservables).pipe(operators_1.map((valuesAndTimes) => {
        let values = valuesAndTimes.map(vt => vt.value);
        var index = 0;
        for (let i = 0; i < valuesAndTimes.length; i++) {
            if (valuesAndTimes[i].date > valuesAndTimes[index].date) {
                index = i;
            }
        }
        return [values, index];
    }));
    return combined;
}
exports.combineLatestObservablesWithTriggeringIndex = combineLatestObservablesWithTriggeringIndex;
//# sourceMappingURL=CombineLatest.js.map