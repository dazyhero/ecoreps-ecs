import { Observable } from "rxjs";
export declare function combineLatestObservablesWithTriggeringIndex(observables: Observable<any>[]): Observable<any[]>;
