"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
String.prototype.allReplace = function (obj) {
    var retStr = this;
    for (var x in obj) {
        retStr = retStr.replace(x, obj[x]);
    }
    return retStr;
};
//# sourceMappingURL=string_extension.js.map