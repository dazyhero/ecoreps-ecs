"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseType = void 0;
var CourseType;
(function (CourseType) {
    CourseType["online"] = "online";
    CourseType["offline"] = "offline";
})(CourseType = exports.CourseType || (exports.CourseType = {}));
//# sourceMappingURL=CourseType.js.map