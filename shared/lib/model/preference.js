"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Preference = void 0;
var Preference;
(function (Preference) {
    Preference["lastSummary"] = "lastsummary";
    Preference["lastAssignment"] = "lastAssignment";
    Preference["lastExam"] = "lastExam";
    Preference["selectedUniversityId"] = "selectedUniversityId";
    Preference["nonce"] = "nonce";
})(Preference = exports.Preference || (exports.Preference = {}));
//# sourceMappingURL=preference.js.map