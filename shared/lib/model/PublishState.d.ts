export declare enum PublishState {
    published = "published",
    draft = "draft"
}
