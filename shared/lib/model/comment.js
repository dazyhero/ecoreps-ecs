"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllCommentVisibilities = exports.CommentVisibility = void 0;
var CommentVisibility;
(function (CommentVisibility) {
    CommentVisibility[CommentVisibility["admin"] = 0] = "admin";
    CommentVisibility[CommentVisibility["published"] = 1] = "published";
    CommentVisibility[CommentVisibility["deleted"] = 2] = "deleted";
})(CommentVisibility = exports.CommentVisibility || (exports.CommentVisibility = {}));
exports.AllCommentVisibilities = [
    CommentVisibility.published,
    CommentVisibility.admin,
    CommentVisibility.deleted,
];
//# sourceMappingURL=comment.js.map