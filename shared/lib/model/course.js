"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateFinalPrice = void 0;
var IPublishState;
(function (IPublishState) {
    IPublishState["published"] = "published";
})(IPublishState || (IPublishState = {}));
function calculateFinalPrice(price, hasBulkDiscount = false, hasReturningCustomerDiscount = false) {
    var result = price.price;
    result -= price.discount_everyone || 0;
    if (hasBulkDiscount) {
        result -= price.bulk_discount || 0;
    }
    if (hasReturningCustomerDiscount) {
        result -= price.discount_returning_customer || 0;
    }
    return Math.max(result, price.minimum_price);
}
exports.calculateFinalPrice = calculateFinalPrice;
//# sourceMappingURL=course.js.map