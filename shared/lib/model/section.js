"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IVideoDisplayMode = exports.IIllustrationSize = exports.defaultTitleForChapterSectionType = exports.classesForChapterSectionType = exports.SectionTypeInfo = exports.SectionImagePosition = exports.AllSectionTypes = exports.AssignmentSectionTypes = exports.ChapterSectionTypes = exports.SectionType = void 0;
var SectionType;
(function (SectionType) {
    SectionType["Definition"] = "Definition";
    SectionType["Rules"] = "Rules";
    SectionType["Rules2"] = "Rules2";
    SectionType["Example"] = "Example";
    SectionType["Steps"] = "Steps";
    SectionType["Tipps"] = "Tipps";
    SectionType["Text"] = "Text";
    SectionType["Rule"] = "Rule";
    SectionType["Tip"] = "Tip";
    SectionType["Derivation"] = "Derivation";
    SectionType["NerdKnowledge"] = "NerdKnowledge";
    SectionType["ExamSolution"] = "ExamSolution";
    SectionType["HighlightedExamSolution"] = "HighlightedExamSolution";
    SectionType["ExamText"] = "ExamText";
})(SectionType = exports.SectionType || (exports.SectionType = {}));
exports.ChapterSectionTypes = [
    SectionType.Text,
    SectionType.Rules,
    SectionType.Definition,
    SectionType.Example,
    SectionType.Steps,
    SectionType.Tipps,
    SectionType.Tip,
    SectionType.Rules2,
    SectionType.Rule,
    SectionType.Derivation,
    SectionType.NerdKnowledge,
];
exports.AssignmentSectionTypes = [
    SectionType.ExamSolution,
    SectionType.HighlightedExamSolution,
    SectionType.ExamText,
];
exports.AllSectionTypes = exports.ChapterSectionTypes.concat(exports.AssignmentSectionTypes);
var SectionImagePosition;
(function (SectionImagePosition) {
    SectionImagePosition[SectionImagePosition["insideBox"] = 0] = "insideBox";
    SectionImagePosition[SectionImagePosition["belowBox"] = 1] = "belowBox";
    SectionImagePosition[SectionImagePosition["aboveBox"] = 2] = "aboveBox";
})(SectionImagePosition = exports.SectionImagePosition || (exports.SectionImagePosition = {}));
class SectionTypeInfo {
    constructor(type) {
        this.type = type;
    }
    get classString() {
        return this.cssClasses.join(' ');
    }
    get boxClassString() {
        return this.boxCssClasses.join(' ');
    }
    get includeInProgress() {
        return [SectionType.ExamText].indexOf(this.type) === -1;
    }
    get includeSections() {
        return [SectionType.ExamText].indexOf(this.type) === -1;
    }
    get defaultTitle() {
        const titles = {
            [SectionType.Definition]: 'Definition',
            [SectionType.Rules]: 'Formel',
            [SectionType.Rules2]: "Formel ohne '!'",
            [SectionType.Example]: 'Beispiel',
            [SectionType.Steps]: 'Vorgehen',
            [SectionType.Tipps]: 'Merke!',
            [SectionType.Tip]: 'Tipp',
            [SectionType.Rule]: 'Regel',
            [SectionType.Derivation]: 'Herleitung',
            [SectionType.NerdKnowledge]: 'Streberwissen',
            [SectionType.ExamSolution]: 'Lösung',
            [SectionType.HighlightedExamSolution]: 'Hervorgehobene Lösung',
            [SectionType.ExamText]: 'Aufgabentext',
        };
        return titles[this.type] || '';
    }
    get expandable() {
        return ([SectionType.ExamSolution, SectionType.HighlightedExamSolution].indexOf(this.type) >= 0);
    }
    get cssClasses() {
        const classes = {
            [SectionType.Definition]: ['definition-body'],
            [SectionType.Rules]: [
                'rules-body',
                'col-md',
                'container-fluid',
                'bodybox',
            ],
            [SectionType.Rules2]: [
                'rules-body',
                'col-md',
                'container-fluid',
                'bodybox',
            ],
            [SectionType.Rule]: [
                'rules-body',
                'col-md',
                'container-fluid',
                'bodybox',
            ],
            [SectionType.Steps]: ['example-box', 'steps'],
            [SectionType.Example]: ['example-box'],
            [SectionType.Tipps]: ['tipps-body', 'container-flex'],
            [SectionType.Tip]: ['tipps-body', 'container-flex', 'tip'],
            [SectionType.Derivation]: ['example-box'],
            [SectionType.NerdKnowledge]: ['tipps-body', 'container-flex', 'nerd-knowledge'],
            [SectionType.ExamSolution]: [],
            [SectionType.HighlightedExamSolution]: ['rules-body', 'container-flex'],
        };
        return classes[this.type] || [];
    }
    get boxCssClasses() {
        const classes = {
            [SectionType.ExamSolution]: ['example-box'],
            [SectionType.HighlightedExamSolution]: ['example-box'],
        };
        return classes[this.type] || [];
    }
    get imagePosition() {
        const positions = {};
        return positions[this.type] || SectionImagePosition.insideBox;
    }
}
exports.SectionTypeInfo = SectionTypeInfo;
function classesForChapterSectionType(sectionType) {
    switch (sectionType) {
        case SectionType.Example:
            return ['example-box'];
        case SectionType.Definition:
            return ['definition-body'];
        case SectionType.Steps:
            return ['example-box'];
        case SectionType.Tipps:
            return ['tipps-body container-flex'];
        case SectionType.Rules:
            return ['rules-body container-flex'];
        case SectionType.Rules2:
            return ['rules-body container-flex'];
        default:
            return [];
    }
}
exports.classesForChapterSectionType = classesForChapterSectionType;
function defaultTitleForChapterSectionType(sectionType) {
    switch (sectionType) {
        case SectionType.Definition:
            return 'Definition';
        case SectionType.Rules:
            return 'Formel';
        case SectionType.Rules2:
            return "Formel ohne '!'";
        case SectionType.Example:
            return 'Beispiel';
        case SectionType.Steps:
            return 'Vorgehen';
        case SectionType.Tipps:
            return 'Merke!';
        case SectionType.Tip:
            return 'Tipp';
        case SectionType.Rule:
            return 'Regel';
        case SectionType.Derivation:
            return 'Herleitung';
        case SectionType.NerdKnowledge:
            return 'Streberwissen';
        default:
            return null;
    }
}
exports.defaultTitleForChapterSectionType = defaultTitleForChapterSectionType;
var IIllustrationSize;
(function (IIllustrationSize) {
    IIllustrationSize[IIllustrationSize["small"] = 0] = "small";
    IIllustrationSize[IIllustrationSize["medium"] = 1] = "medium";
    IIllustrationSize[IIllustrationSize["large"] = 2] = "large";
})(IIllustrationSize = exports.IIllustrationSize || (exports.IIllustrationSize = {}));
var IVideoDisplayMode;
(function (IVideoDisplayMode) {
    IVideoDisplayMode[IVideoDisplayMode["collapsed"] = 0] = "collapsed";
    IVideoDisplayMode[IVideoDisplayMode["expanded"] = 1] = "expanded";
    IVideoDisplayMode[IVideoDisplayMode["playing"] = 2] = "playing";
})(IVideoDisplayMode = exports.IVideoDisplayMode || (exports.IVideoDisplayMode = {}));
//# sourceMappingURL=section.js.map