"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientTypeId = exports.ClientId = void 0;
var ClientId;
(function (ClientId) {
    ClientId[ClientId["ecoreps"] = 100] = "ecoreps";
    ClientId[ClientId["studyPrime"] = 200] = "studyPrime";
})(ClientId = exports.ClientId || (exports.ClientId = {}));
exports.ClientTypeId = {
    100: 'Ecoreps',
    200: 'StudyPrime',
};
//# sourceMappingURL=clientId.js.map