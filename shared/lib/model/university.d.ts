import { ICourse } from './course';
export interface IUniversity {
    _id: any;
    id?: any;
    name: string;
    coverURL: string;
    createdAt?: Date;
    updateAt?: Date;
    courses?: ICourse[];
    url_slug: string;
}
