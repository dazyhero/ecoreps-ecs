import { ICourse } from './course';
import { PaymentProvider } from './PaymentProvider';
export interface IBasicOrderRequest {
    summary: ISummaryRequest;
    expectedTotal: number;
    phoneNumber?: string;
    billing: {
        fullName: string;
    };
}
export interface IPaypalOrderRequest extends IBasicOrderRequest {
}
export interface IOrderRequest extends IBasicOrderRequest {
    billing: {
        fullName: string;
        street: string;
        streetNumber: string;
        city: string;
        plz: string;
        country: string;
    };
}
export declare enum CourseOfferState {
    offered = 0,
    inCart = 1,
    purchased = 2
}
export interface PriceInfo {
    originalValue?: number;
    value: number;
    savingInPercent?: number;
}
export interface IHasPriceInfo {
    price: PriceInfo;
}
export interface IHasOfferState {
    state: CourseOfferState;
}
export interface ICourseItem {
    id: string;
    title: string;
    product_category: string;
}
export interface IAddonOffer extends IHasPriceInfo, IHasOfferState {
    id: string;
    title: string;
    description: string;
}
export interface ISummaryResponse {
    currency: string;
    courseOffers: (IHasOfferState & IHasPriceInfo & ICourseItem)[];
    addonOffers: IAddonOffer[];
    effectiveVouchers?: string[];
    totalVoucherDiscount: number;
    total: PriceInfo;
}
export interface ISummaryRequest {
    course: string;
    selectedCourseIds: string[];
    selectedAddonIds: string[];
    activeVoucher: string;
}
export default interface IOrder extends IBasicOrderRequest {
    _id: any;
    course: string | ICourse;
    userId: string;
    createdAt: number;
    status: OrderStatus;
    statusDescription?: string;
    paymentProviderDescription?: string;
    fullName?: string;
    street?: string;
    streetNumber?: string;
    city?: string;
    plz?: string;
    country?: string;
    externalTransactionId?: string;
    paymentProvider?: PaymentProvider;
    amountInCent: number;
    phoneNumber?: string;
    currency: string;
    summaryResponse: ISummaryResponse;
}
export declare enum OrderStatus {
    Placed = 0,
    Processed = 1,
    CanceledByCustomer = 2,
    CanceledByServiceOperator = 3,
    Paid = 4,
    Confirmed = 5,
    FailedToSendConfirmationError = 6,
    Created = 7
}
export declare class OrderInfo {
    order: IOrder;
    constructor(order: IOrder);
    get statusDescription(): string;
    get paymentProviderDescription(): string;
}
