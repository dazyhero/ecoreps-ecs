"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublishState = void 0;
var PublishState;
(function (PublishState) {
    PublishState["published"] = "published";
    PublishState["draft"] = "draft";
})(PublishState = exports.PublishState || (exports.PublishState = {}));
//# sourceMappingURL=PublishState.js.map