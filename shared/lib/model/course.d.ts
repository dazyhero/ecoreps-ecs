import { IChapter, ProgressType, PreviewMode } from './chapter';
import { IUniversity } from "./university";
import { PublishState } from './PublishState';
import { CourseType } from './CourseType';
export interface IExamDate {
    start: Date;
    title: string;
    color: string;
}
export interface IColumn {
    title: string;
    url_slug: string;
    chapter_style: string;
    progress_tracking: ProgressType;
    show_chapter_title: boolean;
    enforce_hierarchical_chapter_structure: boolean;
    show_chapter_indices: boolean;
    allow_comments?: boolean;
    chapters: IChapter[];
    stats: {
        progressAverageUnderstood: number;
        progressAverageProcessed: number;
        chapterCount: number;
        sectionCount: number;
    };
    lastVisited?: {
        title: string;
        url_slug: string;
    };
}
export interface IPrice {
    price: number;
    discount_everyone: number;
    minimum_price: number;
    bulk_discount?: number;
    discount_returning_customer?: number;
    currency: string;
}
export declare function calculateFinalPrice(price: IPrice, hasBulkDiscount?: boolean, hasReturningCustomerDiscount?: boolean): number;
export interface ICourse {
    _id: any;
    id?: any;
    title: string;
    status: PublishState;
    hasPreview?: boolean;
    coursePreviewType?: PreviewMode;
    isPreview?: boolean;
    coverURL: string;
    university?: IUniversity;
    isPurchased?: boolean;
    url_slug?: string;
    contact_phone?: string;
    contact_mail?: string;
    price: IPrice;
    totalProgress?: number;
    columns: IColumn[];
    exams: IExamDate[];
    exam_date: string;
    type: CourseType;
    whatsapp_service: string;
    product_category: {
        id: string;
        university: string;
        title: string;
        bundle_discount: number;
    };
    stats: {
        chapterCount: number;
        sectionCount: number;
    };
}
export interface IUpdateCourseModel {
    _id: string;
    title: string;
    contact_mail: string;
    contact_phone: string;
    coverURL: string;
}
