import IComment from './comment';
export declare enum SectionType {
    Definition = "Definition",
    Rules = "Rules",
    Rules2 = "Rules2",
    Example = "Example",
    Steps = "Steps",
    Tipps = "Tipps",
    Text = "Text",
    Rule = "Rule",
    Tip = "Tip",
    Derivation = "Derivation",
    NerdKnowledge = "NerdKnowledge",
    ExamSolution = "ExamSolution",
    HighlightedExamSolution = "HighlightedExamSolution",
    ExamText = "ExamText"
}
export declare const ChapterSectionTypes: SectionType[];
export declare const AssignmentSectionTypes: SectionType[];
export declare const AllSectionTypes: SectionType[];
export declare enum SectionImagePosition {
    insideBox = 0,
    belowBox = 1,
    aboveBox = 2
}
export interface ISectionTypeInfo {
    defaultTitle: string;
    imagePosition: SectionImagePosition;
    cssClasses: string[];
    boxCssClasses: string[];
    classString: string;
    boxClassString: string;
    expandable: boolean;
    type: SectionType;
}
export declare class SectionTypeInfo implements ISectionTypeInfo {
    type: SectionType;
    constructor(type: SectionType);
    get classString(): string;
    get boxClassString(): string;
    get includeInProgress(): boolean;
    get includeSections(): boolean;
    get defaultTitle(): string;
    get expandable(): boolean;
    get cssClasses(): string[];
    get boxCssClasses(): string[];
    get imagePosition(): SectionImagePosition;
}
export declare function classesForChapterSectionType(sectionType: SectionType): string[];
export declare function defaultTitleForChapterSectionType(sectionType: SectionType): string;
export declare enum IIllustrationSize {
    small = 0,
    medium = 1,
    large = 2
}
export declare type IIllustrationPixelSize = {
    height: number;
    width: number;
};
export declare enum IVideoDisplayMode {
    collapsed = 0,
    expanded = 1,
    playing = 2
}
export interface IIllustrationWrapper {
    url?: string;
    illustration?: string;
    description?: string;
    size: IIllustrationSize;
    pixelSize: IIllustrationPixelSize;
}
export interface IVideoWrapper {
    url?: string;
    public_id?: string;
    description?: string;
    displayMode: IVideoDisplayMode;
    isVimeo?: boolean;
}
export interface ISection {
    _id: any;
    title?: string;
    hasTitle?: boolean;
    type?: SectionType;
    content?: string;
    images?: IIllustrationWrapper[];
    mobileImages?: IIllustrationWrapper[];
    videos?: IVideoWrapper[];
    fullURLs?: string[];
    editorContent?: string;
    editorTitle?: string;
    comments?: IComment[] | string[];
    stats?: {
        commentCount: number;
    };
}
