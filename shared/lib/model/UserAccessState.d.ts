export interface IUserAccessState {
    email: string;
    access: boolean;
    userId: string;
}
