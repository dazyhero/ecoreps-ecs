"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderInfo = exports.OrderStatus = exports.CourseOfferState = void 0;
const PaymentProvider_1 = require("./PaymentProvider");
var CourseOfferState;
(function (CourseOfferState) {
    CourseOfferState[CourseOfferState["offered"] = 0] = "offered";
    CourseOfferState[CourseOfferState["inCart"] = 1] = "inCart";
    CourseOfferState[CourseOfferState["purchased"] = 2] = "purchased";
})(CourseOfferState = exports.CourseOfferState || (exports.CourseOfferState = {}));
var OrderStatus;
(function (OrderStatus) {
    OrderStatus[OrderStatus["Placed"] = 0] = "Placed";
    OrderStatus[OrderStatus["Processed"] = 1] = "Processed";
    OrderStatus[OrderStatus["CanceledByCustomer"] = 2] = "CanceledByCustomer";
    OrderStatus[OrderStatus["CanceledByServiceOperator"] = 3] = "CanceledByServiceOperator";
    OrderStatus[OrderStatus["Paid"] = 4] = "Paid";
    OrderStatus[OrderStatus["Confirmed"] = 5] = "Confirmed";
    OrderStatus[OrderStatus["FailedToSendConfirmationError"] = 6] = "FailedToSendConfirmationError";
    OrderStatus[OrderStatus["Created"] = 7] = "Created";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
class OrderInfo {
    constructor(order) {
        this.order = order;
    }
    get statusDescription() {
        console.log('stat in is :' + JSON.stringify(this.order) + ' whereas confirmed is ' + OrderStatus.Placed);
        switch (this.order.status) {
            case OrderStatus.CanceledByCustomer:
                return 'widerrufen';
            case OrderStatus.CanceledByServiceOperator:
                return 'abgelehnt';
            case OrderStatus.Confirmed:
                return 'Bestätigt';
            case OrderStatus.Paid:
                return 'Bezahlt';
            case OrderStatus.Created:
                return 'Nicht abgeschlossen';
            case OrderStatus.Placed:
                return 'Bestellung eingegangen';
            case OrderStatus.FailedToSendConfirmationError:
                return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
            default:
                return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
        }
    }
    get paymentProviderDescription() {
        switch (this.order.paymentProvider) {
            case PaymentProvider_1.PaymentProvider.bill:
                return 'Rechnung';
            case PaymentProvider_1.PaymentProvider.paypalCheckout:
                return 'Paypal/Kreditkarte';
            default:
                return 'Serverseitiger Fehler: bitte Admin kontaktieren!';
        }
    }
}
exports.OrderInfo = OrderInfo;
//# sourceMappingURL=order.js.map