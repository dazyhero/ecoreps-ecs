import { IChapter } from './chapter';
export declare enum CommentVisibility {
    admin = 0,
    published = 1,
    deleted = 2
}
export declare const AllCommentVisibilities: CommentVisibility[];
export default interface IComment {
    _id: any;
    author_id: string;
    author_name: string;
    title: string | void;
    text: string;
    date: Date;
    visibility: CommentVisibility;
    replies: IComment[];
    chapter: string | IChapter;
    code?: number;
}
