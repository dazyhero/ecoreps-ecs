export declare enum ClientId {
    ecoreps = 100,
    studyPrime = 200
}
export declare type ClientType = 'Ecoreps' | 'StudyPrime';
export declare const ClientTypeId: Record<number, ClientType>;
