"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChapterTypeInfo = exports.ChapterPriority = exports.ProgressType = exports.PreviewMode = exports.ChapterType = exports.ChapterDisplayMode = void 0;
var ChapterDisplayMode;
(function (ChapterDisplayMode) {
    ChapterDisplayMode[ChapterDisplayMode["hierachical"] = 0] = "hierachical";
    ChapterDisplayMode[ChapterDisplayMode["includeChildren"] = 1] = "includeChildren";
    ChapterDisplayMode[ChapterDisplayMode["includedInParent"] = 2] = "includedInParent";
})(ChapterDisplayMode = exports.ChapterDisplayMode || (exports.ChapterDisplayMode = {}));
var ChapterType;
(function (ChapterType) {
    ChapterType[ChapterType["summaryChapter"] = 0] = "summaryChapter";
    ChapterType[ChapterType["exam"] = 1] = "exam";
    ChapterType[ChapterType["examAssignment"] = 2] = "examAssignment";
    ChapterType[ChapterType["examByTopicChapter"] = 3] = "examByTopicChapter";
    ChapterType[ChapterType["additionalTasks"] = 4] = "additionalTasks";
})(ChapterType = exports.ChapterType || (exports.ChapterType = {}));
var PreviewMode;
(function (PreviewMode) {
    PreviewMode[PreviewMode["closedMode"] = 0] = "closedMode";
    PreviewMode[PreviewMode["membersMode"] = 1] = "membersMode";
    PreviewMode[PreviewMode["publicMode"] = 2] = "publicMode";
})(PreviewMode = exports.PreviewMode || (exports.PreviewMode = {}));
var ProgressType;
(function (ProgressType) {
    ProgressType[ProgressType["none"] = 0] = "none";
    ProgressType[ProgressType["byChapter"] = 1] = "byChapter";
    ProgressType[ProgressType["bySection"] = 2] = "bySection";
})(ProgressType = exports.ProgressType || (exports.ProgressType = {}));
var ChapterPriority;
(function (ChapterPriority) {
    ChapterPriority[ChapterPriority["normal"] = 0] = "normal";
    ChapterPriority[ChapterPriority["highlighted"] = 1] = "highlighted";
})(ChapterPriority = exports.ChapterPriority || (exports.ChapterPriority = {}));
class ChapterTypeInfo {
    constructor(column) {
        this.column = column;
    }
    get hasExamSectionTypes() {
        return this.column.chapter_style != 'summary';
    }
    get progressType() {
        return this.column.progress_tracking;
    }
    get defaultDisplayMode() {
        if (this.forcedDisplayMode) {
            return this.forcedDisplayMode;
        }
        return ChapterDisplayMode.hierachical;
    }
    get showChapterTitle() {
        return this.column.show_chapter_title;
    }
    get forcedDisplayMode() {
        return this.column.enforce_hierarchical_chapter_structure
            ? ChapterDisplayMode.hierachical
            : null;
    }
    get showChapterIndizes() {
        return this.column.show_chapter_indices;
    }
    get canHaveSections() {
        return true;
    }
    get localName() {
        return this.column.title;
    }
}
exports.ChapterTypeInfo = ChapterTypeInfo;
//# sourceMappingURL=chapter.js.map