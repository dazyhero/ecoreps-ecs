"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentProvider = void 0;
var PaymentProvider;
(function (PaymentProvider) {
    PaymentProvider[PaymentProvider["bill"] = 1] = "bill";
    PaymentProvider[PaymentProvider["paypalCheckout"] = 2] = "paypalCheckout";
})(PaymentProvider = exports.PaymentProvider || (exports.PaymentProvider = {}));
//# sourceMappingURL=PaymentProvider.js.map