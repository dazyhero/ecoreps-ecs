"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllPossibleProgressStates = exports.ProgressState = void 0;
var ProgressState;
(function (ProgressState) {
    ProgressState[ProgressState["undefined"] = 0] = "undefined";
    ProgressState[ProgressState["notUnderstood"] = 1] = "notUnderstood";
    ProgressState[ProgressState["partlyUnderstood"] = 2] = "partlyUnderstood";
    ProgressState[ProgressState["fullyUnderstood"] = 3] = "fullyUnderstood";
})(ProgressState = exports.ProgressState || (exports.ProgressState = {}));
exports.AllPossibleProgressStates = [
    ProgressState.undefined,
    ProgressState.notUnderstood,
    ProgressState.partlyUnderstood,
    ProgressState.fullyUnderstood,
];
//# sourceMappingURL=profile.js.map