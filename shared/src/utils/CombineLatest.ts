import {Observable,combineLatest} from "rxjs";
import {map} from "rxjs/operators";


export function combineLatestObservablesWithTriggeringIndex(observables: Observable<any>[]):Observable<any[]>{
    let timedObservables = observables.map(obs => obs.pipe(map( val => {return {value: val, date: new Date().getTime()}})));
    let combined = combineLatest(timedObservables).pipe(map((valuesAndTimes) => {
            let values = valuesAndTimes.map(vt => vt.value);
            var index = 0;
            for (let i=0; i < valuesAndTimes.length;i++) {
                if (valuesAndTimes[i].date > valuesAndTimes[index].date) {
                    index = i;
                }
            }
            return [values, index];
        }
    ));
    return combined;
}