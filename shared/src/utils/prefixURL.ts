export interface IObjectKeyReplacement {
    key: string,
    target: string,
    isArray?: boolean
}


export function prefixUrlForObject(object: any, urlMapper: (value: string) => string, replacements: IObjectKeyReplacement[]) {
    console.log("calls uni replace with object: "+JSON.stringify(object));
    for (let r of replacements) {
        //get value
        var value: any = recursiveGet(object,r.key.split("."));
        let newValue = (r.isArray) ? value.map(i =>  urlMapper(i) ) : urlMapper(value);
        recursiveSet(object, r.target.split("."), newValue);
    }
    return object;
}


function recursiveGet(object, nestedKeys: string[]): any {
    console.log("calls uni replace get with object: "+JSON.stringify(object)+" and keys: "+JSON.stringify(nestedKeys));
    if(!object){
        return null;
    }
    if (nestedKeys.length < 1) {
        return null;
    }
    let firstKey = nestedKeys[0];
    nestedKeys.shift();
    let value = (nestedKeys.length == 0) ? object[firstKey] : recursiveGet(object[firstKey], nestedKeys);
    return value;
}

function recursiveSet(object, nestedKeys: string[], value: any): any {
    if (nestedKeys.length < 1) {
        return null;
    }
    let firstKey = nestedKeys[0];
    nestedKeys.shift();
    let newObject = (nestedKeys.length == 0) ? value : recursiveSet(object[firstKey], nestedKeys, value);
    object[firstKey] = newObject;
    return object;
}