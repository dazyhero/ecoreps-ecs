export {};
declare global {
  interface String {
    allReplace(input: object): string;
  }
}
String.prototype.allReplace = function(obj) {
  var retStr = this;
  for (var x in obj) {
    retStr = retStr.replace(x, obj[x]);
  }
  return retStr;
};
