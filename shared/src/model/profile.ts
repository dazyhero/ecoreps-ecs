import { ICourse } from './course';
import { IUniversity } from "./university";

export enum ProgressState {
  undefined = 0,
  notUnderstood = 1,
  partlyUnderstood = 2,
  fullyUnderstood = 3,
}
export const AllPossibleProgressStates = [
  ProgressState.undefined,
  ProgressState.notUnderstood,
  ProgressState.partlyUnderstood,
  ProgressState.fullyUnderstood,
];

export interface ISectionProgress {
  section: string;
  state: ProgressState;
}

export interface IProgressChapterEntry {
  chapter: string;
  sections: ISectionProgress[];
  state: ProgressState;
}

export interface IColumnProgress {
  columnId: string;
  progressUnderstood: number;
  progressProcessed: number;
  progressUnderstoodOthers?: number;
  progressProcessedOthers?: number;
  chapterProgress: IProgressChapterEntry[];
  includeInAggregations?: boolean;
}

export interface IProgressAggregation {
  progressProcessed: number;
  progressUnderstood: number;
}
export interface IProgress {
  course_id: any;
  progressTotal: number;
  progressProcessedTotal: number;
  columnProgress: IColumnProgress[];
}

export default interface IProfile {
  userId: string;
  progress: IProgress[];
  statusDescription?: string;
  last_login: number;
  bookmarkedCourses: ICourse[];
  mainUniversity?: { id: string, url_slug: string, name: string };
  anonymousId?: string;
}
 
export interface ICompletionRequest {
  state: ProgressState;
  chapterId: string;
  sectionId: string;
  unset?: boolean;
}
