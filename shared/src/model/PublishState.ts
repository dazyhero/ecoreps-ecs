export enum PublishState {
  published = 'published',
  draft = 'draft',
}
